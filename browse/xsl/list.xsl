<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
xmlns:xalan="http://xml.apache.org/xalan"
xmlns:redirect="org.apache.xalan.lib.Redirect"
exclude-result-prefixes="xalan" 
extension-element-prefixes="redirect">
<!-- creates alphabetical list of links from hw list -->
	
<xsl:output method="html" indent="yes"/>

<xsl:param name="cat" select="'notPassed'"/>
<xsl:param name="letter" select="'notPassed'"/>

<xsl:variable name="vCat">
	<xsl:choose>
		<xsl:when test="$cat = 'healthTopics'">ht</xsl:when>
		<xsl:when test="$cat = 'medications'">meds</xsl:when>
		<xsl:when test="$cat = 'supportGroups'">shc</xsl:when>
		<xsl:when test="$cat = 'medicalTests'">tests</xsl:when>
		<xsl:when test="$cat = 'CAM'">cam</xsl:when>
		<xsl:otherwise>all</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="vLtr">
	<xsl:choose>
		<xsl:when test="$letter = 'num'">numbers</xsl:when>
		<xsl:otherwise><xsl:value-of select="translate($letter, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/></xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:template match="/">
	<xsl:if test="$letter != 'notPassed'">
		<!-- go get the file and extract data -->
		<xsl:variable name="vFileLoc" select="concat('../../hwxml/list/', $vCat, '/', $vLtr, '.xml')"/>
		<xsl:for-each select="document($vFileLoc)">
		
			<!-- build links to alphabetical named anchors -->
			<xsl:variable name="subCount">
				<xsl:choose>
					<xsl:when test="hw.list/list.subgroup[@letter = '##']">14</xsl:when>
					<xsl:otherwise>13</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<div id="brwsSubAlpha">
				<ul>
					<xsl:for-each select="hw.list/list.subgroup[position() &lt;= $subCount and not(@letter = '##')]">
						<li><a href="{concat('#',@letter)}"><xsl:value-of select="@letter"/></a></li>
					</xsl:for-each>
				</ul>
				<xsl:if test="count(hw.list/list.subgroup) &gt;13">
					<ul>
						<xsl:for-each select="hw.list/list.subgroup[position() &gt; $subCount]">
						<li><a href="{concat('#',@letter)}"><xsl:value-of select="@letter"/></a></li>
						</xsl:for-each>
					</ul>
				</xsl:if>
			</div>
			<!-- build list of document links -->
			<xsl:for-each select="hw.list/list.subgroup">
				<a name="{@letter}"/>
				<h3 class="normaltextb1" style="margin-bottom:0;">
					<xsl:choose>
						<xsl:when test="@letter = '##'"><xsl:text> </xsl:text></xsl:when>
						<xsl:otherwise><xsl:value-of select="@letter"/></xsl:otherwise>
					</xsl:choose>
				</h3>
				<ul style="margin:0;padding:0;list-style-type:none;">
				<xsl:for-each select="subgroup.item">
					<xsl:variable name="vPath" select="concat('/kbase/topic.jhtml?docId=',@document-href,'&amp;secId=',@section-href)"/>
					<li><a href="{$vPath}"><xsl:value-of select="."/></a></li>
				</xsl:for-each>
				</ul>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>