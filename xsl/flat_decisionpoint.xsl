<?xml version="1.0"?>
<!--
================================================================================
Purpose: Generate an XHTML non interactive Decision Point from the Knowledgebase
================================================================================
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml">

  <!--
================================================================================
 Global Variables
================================================================================
  
  -->
  
  <xsl:variable name="gvPrinterFriendlyNote">
    <xsl:text>Note: The "printer friendly" document will not contain all the information available in the online document some Information (e.g. cross-references to other topics, definitions or medical illustrations) is only available in the online version.</xsl:text>
  </xsl:variable>

  <!--
================================================================================
 Flat DP
================================================================================
-->
  <xsl:template match="doc.section" mode="FlatTitles">
    <li>
      <xsl:value-of select="section.meta-data/meta-data.title"/>
    </li>
  </xsl:template>


  <xsl:template name="HwFlatDPLayout">
    <xsl:param name="pRoot"/>
    <!-- START FLAT DP -->

    <div id="HwFlatDecisionPointWrapper">
      <div id="HwFlatDecisionPoint">
        <h2  class='HwFlatDecisionPointHeadline'>
          <xsl:value-of select="/hw.doc/doc.meta-data/meta-data.title"/>
        </h2>
        <div id="HwFlatHeadlineSubtext">
            You can use this information to talk with your
            doctor or loved ones about your decision.
        </div>
        <div id='HwFlatDecisionPointSections'>
          <ol>
            <xsl:apply-templates mode="FlatTitles" select="/hw.doc/doc.sections/doc.section[starts-with(section.std/@type, 'Dp.')]"/>
          </ol>
        </div>
        <div id='HwFlatDecisionPointGetTheFacts' class="HwFlatDecisionPointTabSection">
          <h3>
            <xsl:text>1. </xsl:text>
            <xsl:call-template name="GetSectionName">
                <xsl:with-param name="idx">
                    <xsl:value-of select="'1'"/>
                </xsl:with-param>
            </xsl:call-template>
          </h3>
          <xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.GetTheFacts']" mode="HwFlat"/>
        </div>
        <div id='HwFlatDecisionPointCompareOptions' class="HwFlatDecisionPointTabSection">
          <h3>
            <xsl:text>2. </xsl:text>
            <xsl:call-template name="GetSectionName">
                <xsl:with-param name="idx">
                    <xsl:value-of select="'2'"/>
                </xsl:with-param>
            </xsl:call-template>
          </h3>
          <xsl:apply-templates select="/hw.doc/doc.sections/doc.section[section.std/@type='Dp.CompareOptions']" mode="HwFlatCompareOptions"/>
        </div>
        <div id='HwFlatDecisionPointYourFeelings' class="HwFlatDecisionPointTabSection">
          <h3>
            <xsl:text>3. </xsl:text>
            <xsl:call-template name="GetSectionName">
                <xsl:with-param name="idx">
                    <xsl:value-of select="'3'"/>
                </xsl:with-param>
            </xsl:call-template>
          </h3>
          <xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.YourFeelings']" mode="HwFlat"/>
        </div>
        <div class="HwFlatStopFloat">
          <xsl:text> </xsl:text>
        </div>
        <div id='HwFlatDecisionPointYourDecision' class="HwFlatDecisionPointTabSection">
          <h3>
            <xsl:text>4. </xsl:text>
            <xsl:call-template name="GetSectionName">
                <xsl:with-param name="idx">
                    <xsl:value-of select="'4'"/>
                </xsl:with-param>
            </xsl:call-template>
          </h3>
          <xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.YourDecision']" mode="HwFlat"/>
        </div>
        <div id='HwFlatDecisionPointQuizYourself' class="HwFlatDecisionPointTabSection">
          <h3>
            <xsl:text>5. </xsl:text>
            <xsl:call-template name="GetSectionName">
                <xsl:with-param name="idx">
                    <xsl:value-of select="'5'"/>
                </xsl:with-param>
            </xsl:call-template>
          </h3>
          <xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.QuizYourself']"  mode="HwFlat"/>
        </div>
        <xsl:call-template name="HwFlatReferences"/>
        <div id="HwFlatDecisionPointDisclaimer" class="HwFlatDecisionPointDisclaimer">
          <hr class="HwFlatDisclaimerNoteTextDivider"/>
          <span class="HwFlatDisclaimerNoteText">
            <xsl:value-of select="$gvPrinterFriendlyNote"/>
          </span>
        </div>
      </div>
    </div>
    <!-- END FLAT DP -->
  </xsl:template>

  <xsl:template match="doc.section" mode="FlatTitles">
    <li>
      <xsl:value-of select="section.meta-data/meta-data.title"/>
    </li>
  </xsl:template>

  <xsl:template match="p[@class='HwSectionTitle']" mode='HwFlat' priority='10'/>

  <xsl:template match='/hw.doc/doc.sections/doc.section/section.std/div[@class = "HwDecisionPointContent"]'  mode="HwFlat">
    <div class="HwFlatDecisionPointContent">
      <xsl:apply-templates mode="HwFlat"/>
    </div>
  </xsl:template>

  <xsl:template match="h3[@class = 'HwFAQQuestion']" mode="HwFlat">
    <span class='HwFlatFAQQuestion'>
      <xsl:value-of select='text()'/>
    </span>
  </xsl:template>

  <!--<xsl:template match="*[@class]" mode="HwFlat">-->
  <xsl:template match="*[@class]|*[@id]" mode="HwFlat">
    <xsl:element name="{name()}">
      <xsl:for-each select="@*">
        <xsl:choose>
          <xsl:when test="local-name(.) = 'class'">
            <xsl:attribute name="class">
              <xsl:text>HwFlat</xsl:text>
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="local-name(.) = 'id'">
            <xsl:attribute name="id">
              <xsl:text>HwFlat</xsl:text>
              <xsl:value-of select="."/>
            </xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
      <xsl:apply-templates mode="HwFlat"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="p[@class='HwSubHeadline']|div[@class='HwFAQSTitle']" mode="HwFlat">
    <xsl:element name="h4">
      <xsl:apply-templates mode="HwFlat"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="div[@id='HwFAQs']" mode="HwFlat">
    <xsl:element name="{name()}">
       <xsl:copy-of select="@*"/>
       <xsl:attribute name="id">HwFlatFAQS</xsl:attribute>
       <div class='HwFAQsTitle'>
          FAQs
       </div>
       <xsl:apply-templates />
    </xsl:element>
  </xsl:template>

  <!-- For any of the XHTML elements, we just want to pass them thru. -->
  <xsl:template match="a|h3|h4|h5|h6|li|ul|ol|dl|dd|p|div|blockquote|hr|img|col|td|tr|th|tbody|thead|tfoot|table|caption|colgroup|span|i|b|u|sub|sup|object|script|param|embed|textarea|input" mode="HwFlat">
    <xsl:element name="{name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="HwFlat"/>
    </xsl:element>
  </xsl:template>

  <!-- Render breaks with no space in element so extra blanks lines are not generated by IE -->
  <xsl:template match="br">
    <br />
  </xsl:template>


<xsl:template match="link.content" mode="HwFlat">
  <xsl:variable name="vPath">
    <xsl:choose>
      <xsl:when test="parent::sup[@class = 'Reference']">
        <!-- For references, link to the reference section (as opposed to the exact reference) -->
        <xsl:value-of select="concat('#', concat(@document-href, '-Bib'))"/>
      </xsl:when>
      <xsl:otherwise>
       <!-- <xsl:call-template name="PathUtils_LinkPath">
          <xsl:with-param name="pTargetDocType" select="@document-type"/>
          <xsl:with-param name="pTargetDocHWID" select="@document-href"/>
          <xsl:with-param name="pTargetSecHWID" select="@section-href"/>
          <xsl:with-param name="pTargetNameHref" select="@name-href"/>
        </xsl:call-template> -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <a>
    <xsl:if test="parent::sup[@class = 'Reference']">
      <xsl:attribute name="class">
        <xsl:text>HwLinkReference</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="rel">
        <xsl:value-of select="concat(@name-href, '-Cit')"/>
      </xsl:attribute>
    </xsl:if>

    <xsl:if test="@document-type = 'Definition' or         
						@document-type = 'MultiMedia' or          
						@document-type = 'Calculator'">
      <xsl:variable name="documentType">
        <xsl:choose>
          <xsl:when test="@link-type='calculator'">
            <xsl:text>Calculator</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@document-type"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:attribute name="class">
        <xsl:text>HwLink</xsl:text>
        <xsl:value-of select="$documentType"/>
        <!-- For the "What is a PDF link?" links, also apply other class enable removing the icon from link in CSS -->
        <xsl:if test="text()='PDF'">
          <xsl:text> HwLinkPDF</xsl:text>
        </xsl:if>
      </xsl:attribute>
      <xsl:attribute name="rel">
        <xsl:value-of select="$documentType"/>
      </xsl:attribute>
      <xsl:attribute name="href">
        <xsl:value-of select="$vPath"/>
      </xsl:attribute>
    </xsl:if>

    <xsl:attribute name="href">
      <xsl:choose>
        <xsl:when test="$gvDocType='Nord'">
          <xsl:text>#</xsl:text>
          <xsl:value-of select="@section-href"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$vPath"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:choose>
      <!-- if one of these link types, add helper spans to aid CSS work
             with image icons. -->
      <xsl:when test="@document-type = 'Definition' or         
								@document-type = 'MultiMedia' or          
								@document-type = 'Calculator'">
        <span class="HwLinkText">
          <xsl:apply-templates/>
        </span>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </a>
  </xsl:template>

  <!-- Remove <img> tags for definitions/multimedia/calculator (images are instead added with css)  -->
  <xsl:template match="link.content[img and (@document-type = 'Definition' or         
												@document-type = 'MultiMedia' or          
												@link-type = 'calculator')]" mode="HwFlat">
  </xsl:template>


  <xsl:template match="table[@align]" mode="HwFlat">
    <xsl:element name="{name()}">
      <xsl:for-each select="@*">
        <xsl:choose>
          <xsl:when test="local-name(.) = 'align'">
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
      <xsl:apply-templates mode="HwFlat" />
    </xsl:element>
  </xsl:template>



  <!--
================================================================================
 Compare Options Templates
================================================================================
-->

  <xsl:template match="/hw.doc/doc.sections/doc.section" mode="HwFlatCompareOptions">
    <div id='HwFlatHwComparisonTable'>
      <xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.CompareOptions']/*" mode="HwFlat"/>
      <xsl:call-template name="HwFlatPersonalStories">
        <xsl:with-param name="personalStoriesHwid" select="/hw.doc/doc.prebuilt/prebuilt.navstack/navstack.entries/navstack.entry[. = 'Personal Stories']/@section-href"/>
      </xsl:call-template>
    </div>
  </xsl:template>

  <xsl:template match="div[@id='HwComparisonOptions']" mode="HwFlat">
    <xsl:variable name="vInvolvedQuestion">
      <xsl:value-of select="div[@class='HwComparisonOption'][1]/div[@class='HwOptionInvolved']/h4"/>
    </xsl:variable>
    <xsl:variable name="vBenefitsQuestion">
      <xsl:value-of select="div[@class='HwComparisonOption'][1]/div[@class='HwOptionBenefits']/h4"/>
    </xsl:variable>
    <xsl:variable name="vSideEffectsQuestion">
      <xsl:value-of select="div[@class='HwComparisonOption'][1]/div[@class='HwOptionRisks']/h4"/>
    </xsl:variable>
    <xsl:element name="table">
      <xsl:attribute name="class">HwFlatHwComparisonQuestions</xsl:attribute>
      <xsl:for-each select="//div[@class='HwComparisonOption']">
        <xsl:variable name="vPos" select="position()"/>
        <xsl:if test="$vPos mod 2 = 1">
          <tr>
            <th>
              <xsl:text> </xsl:text>
              <!--<xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>-->
            </th>
            <th>
              <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos]/span[@class='HwOptionTitle']" mode="HwFlat"/>
            </th>
            <xsl:if test="//div[@class='HwComparisonOption'][$vPos+1]">
              <th>
                <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos+1]/span[@class='HwOptionTitle']" mode="HwFlat"/>
              </th>
            </xsl:if>
          </tr>
          <tr>
            <td class="HwFlatCompareOptionsQuestionCol">
              <xsl:value-of select="$vInvolvedQuestion"/>
            </td>
            <td class="HwFlatCompareOptionsCol">
              <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos]/div[@class='HwOptionInvolved']" mode="HwFlat"/>
            </td>
            <xsl:if test="//div[@class='HwComparisonOption'][$vPos+1]">
              <td class="HwFlatCompareOptionsCol">
                <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos+1]/div[@class='HwOptionInvolved']" mode="HwFlat"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <td class="HwFlatCompareOptionsQuestionCol">
              <xsl:value-of select="$vBenefitsQuestion"/>
            </td>
            <td class="HwFlatCompareOptionsCol">
              <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos]/div[@class='HwOptionBenefits']" mode="HwFlat"/>
            </td>
            <xsl:if test="//div[@class='HwComparisonOption'][$vPos+1]">
              <td class="HwFlatCompareOptionsCol">
                <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos+1]/div[@class='HwOptionBenefits']" mode="HwFlat"/>
              </td>
            </xsl:if>
          </tr>
          <tr>
            <td class="HwFlatCompareOptionsQuestionCol">
              <xsl:value-of select="$vSideEffectsQuestion"/>
            </td>
            <td class="HwFlatCompareOptionsCol">
              <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos]/div[@class='HwOptionRisks']" mode="HwFlat"/>
            </td>
            <xsl:if test="//div[@class='HwComparisonOption'][$vPos+1]">
              <td class="HwFlatCompareOptionsCol">
                <xsl:apply-templates select="//div[@class='HwComparisonOption'][$vPos+1]/div[@class='HwOptionRisks']" mode="HwFlat"/>
              </td>
            </xsl:if>
          </tr>

        </xsl:if>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template match="div[@class='HwOptionInvolved']/h4|div[@class='HwOptionBenefits']/h4|div[@class='HwOptionRisks']/h4" mode="HwFlat" >
      <!-- suppress these -->
  </xsl:template>



    <!--
================================================================================
 Personal Stories Templates
================================================================================
-->

  <xsl:template match="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.CompareOptions']/h4" mode="HwFlat">
    <h4>
      <xsl:value-of select="text()"/>
    </h4>
    <p>
      <xsl:value-of select="following-sibling::p[1]"/>
    </p>
  </xsl:template>
  
  <xsl:template match="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.CompareOptions']/p[1]" mode="HwFlat">
    <xsl:choose>
      <xsl:when test="contains(text(), 'Are you interested')">
        <xsl:text/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates mode="HwFlat"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="HwFlatPersonalStories">
    <xsl:param name="personalStoriesHwid"/>
    <div class='HwFlatPersonalStoriesContent'>
      <xsl:apply-templates select="/hw.doc/doc.sections/doc.section[@hwid-content= $personalStoriesHwid]/section.std/div[@class='HwAboutPersonalStories']" mode="HwFlat"/>
    </div>
    <div class="HwFlatStories">
      <div class='HwFlatPersonalStoriesContent'>
        <xsl:for-each select="/hw.doc/doc.sections/doc.section[@hwid-content=$personalStoriesHwid]/section.std/div[@id='HwPersonalStories']/div[@class='HwPersonalStory']">
          <xsl:apply-templates select="div[@class='HwPersonalStoryBody']" mode="HwFlat"/>
          <xsl:apply-templates select="div[@class='HwPersonalStoryIdentity']" mode="HwFlat"/>
        </xsl:for-each>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="div[@class='HwPersonalStoryBody']" mode="HwFlat">
    <xsl:element name="{name()}">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="class">HwFlatStoryBody</xsl:attribute>
      <xsl:apply-templates select="p"  mode="HwFlatStoryBodyQuotes"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="p" mode="HwFlatStoryBodyQuotes">
    <xsl:element name="p">
      <xsl:text>"</xsl:text>
      <xsl:value-of select="text()"/>
      <xsl:text>"</xsl:text>
    </xsl:element>
  </xsl:template>

  <xsl:template match="div[@class='HwPersonalStoryIdentity']/p" mode="HwFlat">
    <p>
      <xsl:text>— </xsl:text>
      <xsl:value-of select="text()"/>
    </p>
  </xsl:template>

  <!--
================================================================================
 Slider Templates
================================================================================
-->
  <xsl:template match="div[@class = 'HwOptionsHeader']" mode="HwFlat">
    <div class="HwFlatSliderBackground">
      <xsl:apply-templates mode="HwFlat"/>
    </div>
    <div class="HwFlatStopFloat">
      <xsl:text> </xsl:text>
    </div>
  </xsl:template>


  <xsl:template match="div[contains(@class,'HwMultiOptionSlider')]" mode="HwFlat">
    <xsl:variable name="vVarId" select="'HwFlatSliderValue_'"/>
    <div class="HwFlatSliderBackground" >
      <xsl:apply-templates select="div[@class = 'HwSliderText']" mode="HwFlat"/>
      <xsl:element name="table">
        <xsl:attribute name="class">HwFlatSliderTable</xsl:attribute>
        <tr class="HwFlatSliderTableRow">
          <td class="HwFlatSliderTableCell">
            <label>&#160;</label>
          </td>
          <td class="HwFlatSliderTableCell">
            <label>&#160;</label>
          </td>
          <td class="HwFlatSliderTableCell">
            <label>&#160;</label>
          </td>
          <td class="HwFlatSliderTableCell">
            <label>&#160;</label>
          </td>
          <td class="HwFlatSliderTableCell">
            <label>&#160;</label>
          </td>
          <xsl:if test="not(contains(@class,'HwMultiOptionSliderLeft'))">
            <td class="HwFlatSliderTableCell">
              <label>&#160;</label>
            </td>
            <td class="HwFlatSliderTableCell">
              <label>&#160;</label>
            </td>
          </xsl:if>
        </tr>
      </xsl:element>
      <xsl:apply-templates select="div[@class = 'HwSliderLabel']" mode="HwFlat"/>
    </div>
  </xsl:template>

  <xsl:template match="div[@class = 'HwSliderText'] | div[@class = 'HwSliderLabel']" mode="HwFlat">
    <xsl:element name="{local-name(.)}">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="class">HwFlatSliderText</xsl:attribute>
      <xsl:apply-templates mode="HwFlat">
        <xsl:with-param name="pQuestionNumber">
          <xsl:value-of select="count(../../preceding-sibling::div[@class='HwQuizQuestion']) + 1"/>.
        </xsl:with-param>
      </xsl:apply-templates>
    </xsl:element>
    <div class="HwFlatStopFloat">
      <xsl:text> </xsl:text>
    </div>
  </xsl:template>

  <xsl:template match="p[@class = 'HwOptionTextRight'] | p[@class = 'HwOptionTextLeft']" mode="HwFlat">
    <xsl:element name="{local-name(.)}">
      <xsl:variable name="className" select="@class"/>
      <xsl:attribute name="class">
        <xsl:text>HwFlat</xsl:text>
        <xsl:value-of select="$className"/>
      </xsl:attribute>
      <xsl:apply-templates  mode="HwFlat"/>
    </xsl:element>
  </xsl:template>

  <!-- These should not be an empty element, so force a space. -->
  <xsl:template match="textarea[not(node())]" mode="HwFlat">
    <div>
      <xsl:element name="table">
        <xsl:attribute name="class">HwFlatSliderTable</xsl:attribute>
        <tr class="HwFlatSliderTableRow">
          <td class="HwFlatCommentCell">
            <label>&#160;</label>
          </td>
        </tr>
      </xsl:element>
    </div>
  </xsl:template>


  <xsl:template match="textarea[contains(@class,'HwSliderCommentLeft')]|textarea[contains(@class,'HwSliderCommentRight')]" mode="HwFlat">
      <xsl:if test="contains(@class, 'Left')">
        <div>
          <xsl:element name="table">
            <xsl:attribute name="class">HwFlatSliderTable</xsl:attribute>
            <tr class="HwFlatSliderTableRow">
              <td class="HwFlatCommentCell">
                <label>&#160;</label>
              </td>
            </tr>
          </xsl:element>
        </div>
      </xsl:if>
  </xsl:template>

  <xsl:template match="textarea[@id='HwConcernsText']" mode="HwFlat">
    <div>
      <xsl:element name="table">
        <xsl:attribute name="class">HwFlatSliderTable</xsl:attribute>
        <tr class="HwFlatSliderTableRow">
          <td class="HwFlatCommentCell">
            <label>&#160;</label>
          </td>
        </tr>
      </xsl:element>    
    </div>
  </xsl:template>
  <xsl:template match="div[@id='HwFAQS']" mode="HwFlat">
    <xsl:element name="{local-name(.)}">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="id">HwFlatFAQS</xsl:attribute>
      <xsl:apply-templates mode="HwFlat"/>
    </xsl:element>
  </xsl:template>

  <!--
================================================================================
 Quiz Templates
================================================================================
-->
  <xsl:template match="div[@class='HwQuizQuestion']/div[@class='HwBooleanQuestion']" mode="HwFlat">
    <p class="HwFlatQuestionText">
      <xsl:value-of select="count(../preceding-sibling::div[@class='HwQuizQuestion']) + 1"/>.
      <xsl:value-of select="p[@class='HwQuestionText']"/>
    </p>
    <div class="HwFlatInputButton">
      <ul>
    <xsl:apply-templates select="input[@type='button']" mode="HwFlat">
      <xsl:with-param name="pGroupName">
        <xsl:value-of select="@id"/>
      </xsl:with-param>
    </xsl:apply-templates>
      </ul>
    </div>
  </xsl:template>

  <xsl:template match="input[@type='button'][@value='Yes' or @value='No']" mode="HwFlat" priority="10">
    <xsl:param name="pGroupName" select="'group'"/>
    <li>
    <xsl:element name="{local-name(.)}">
      <xsl:copy-of select="@*"/>
      <xsl:if test="@id">
        <xsl:attribute name="id">
          <xsl:value-of select="concat('HwFlat', @id)"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="name">
        <xsl:value-of select="$pGroupName"/>
      </xsl:attribute>
      <xsl:attribute name="type">radio</xsl:attribute>
      <xsl:attribute name="class">HwFlatQuizRadioButton</xsl:attribute>
      
    </xsl:element>
    <label>
      <xsl:attribute name="for">
        <xsl:value-of select="concat('HwFlat', @id)"/>
      </xsl:attribute>
      <xsl:value-of select="@value"/>
    </label>
    </li>  
   
  </xsl:template>

  

  <xsl:template match="ul/li[@class='HwAnswer HwCorrectAnswer']" mode="HwFlat">
    <li class="HwAnswer HwCorrectAnswer">
      <input type="radio" checked="checked"/>
      <b>
        <span class="HwSelectedOption"> <xsl:value-of select="span[@class='HwAnswerText']"/>
        </span>
      </b>
    </li>
  </xsl:template>

  <xsl:template match="div[contains(@class,'HwMultipleChoiceQuestion')]" mode="HwFlat">
    <xsl:element name="{local-name(.)}">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="class">HwFlatMultipleChoiceQuestion</xsl:attribute>
      <xsl:attribute name="id">
        <xsl:value-of select="concat('HwFlat', @id)"/>
      </xsl:attribute>
      <xsl:apply-templates mode="HwFlat">
        <xsl:with-param name="pQuestionNumber">
          <xsl:value-of select="count(../preceding-sibling::div[@class='HwQuizQuestion']) + 1"/>.
        </xsl:with-param>
      </xsl:apply-templates>
      <div class="HwFlatSelectedAnswer">
          <xsl:attribute name="id">
              <xsl:value-of select="concat('HwFlatSelectedAnswer', @id)"/>
          </xsl:attribute>
          <xsl:text> </xsl:text>
      </div>
      <xsl:if test="count(div/ul/li[contains(@class, 'HwCorrectAnswer')]/span[@class ='HwAnswerResultsText']) > 0">
        <div>
          <span class="HwAnwerResultsText">
            <xsl:value-of select="div/ul/li[contains(@class, 'HwCorrectAnswer')]/span[@class ='HwAnswerResultsText']"/>
          </span>
        </div>
      </xsl:if>
    </xsl:element>
  </xsl:template>

  <xsl:template match="p[contains(@class, 'HwQuestionText')]" mode="HwFlat">
    <xsl:param name="pQuestionNumber" select="''"/>
    <p>
      <xsl:attribute name="class">HwFlatQuestionText</xsl:attribute>
      <xsl:value-of select="$pQuestionNumber"/>
      <xsl:value-of select="text()"/>
    </p>
  </xsl:template>

  <xsl:template match="span[@class='HwAnswerResultsText']" mode="HwFlat"/>

  <xsl:template match="li[contains(@class,'HwCorrectAnswer')]" mode="HwFlatCorrectAnswer">
    <xsl:value-of select="span[@class='HwAnswerResultsText']/text()"/>
  </xsl:template>

  <xsl:template match="input[@id]" mode="HwFlat" priority="5">
    <xsl:element name="{local-name(.)}">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="id">
        <xsl:value-of select="concat('HwFlat', @id)"/>
      </xsl:attribute>
    </xsl:element>
  </xsl:template>

  <xsl:template match="span[@class='HwAnswerText']" mode="HwFlat">
    <xsl:element name="{local-name(.)}">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="class">
        <xsl:value-of select="concat('HwFlat', @class)"/>
      </xsl:attribute>
      <xsl:apply-templates mode="HwFlat"/>
    </xsl:element>

  </xsl:template>

  <xsl:template match="div[@id='HwCheckYourFacts']/p[1]|div[@id='HwYourChoices']/p[1]|div[@id='HwCertainty']/p[1]" mode="HwFlat">
    <h4>
      <xsl:apply-templates mode="HwFlat"/>
    </h4>
  </xsl:template>

  <xsl:template match="div[@class='HwQuizQuestion']/div[@class='HwConcerns']/p[@class='HwQuestionText']" mode="HwFlat">
    <p class="HwFlatQuestionText">
      <xsl:value-of select="count(../../preceding-sibling::div[@class='HwQuizQuestion']) + 1"/>.
      <xsl:value-of select="text()"/>
    </p>
  </xsl:template>
  <!--
================================================================================
 Reference Template
================================================================================
-->
  <xsl:template name="HwFlatReferences">
    <div class='HwFlatReferenceContainer'>
      <span class='HwFlatReferenceLabel'>Credits and references</span>
      <div class='HwFlatReferenceContent'>
        <div class='HwFlatCreditsTitle'>Credits</div>
          <!-- temporary until credits, etc. come in xml docs -->
          <xsl:if test="count(/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='author-name']) > 0">
              <table class='HwFlatCredits'>
                  <xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='author-name']">
                      <tr>
                          <td class='HwFlatCreditsColumn1'>Author</td>
                          <td>
                              <xsl:value-of select='text()'/>
                          </td>
                      </tr>
                  </xsl:for-each>
                  <xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='editor-name']">
                      <tr>
                          <td class='HwFlatCreditsColumn1'>Editor</td>
                          <td>
                              <xsl:value-of select='text()'/>
                          </td>
                      </tr>
                  </xsl:for-each>
                  <xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='associate-editor-name']">
                      <tr>
                          <td class='HwFlatCreditsColumn1'>Associate Editor</td>
                          <td>
                              <xsl:value-of select='text()'/>
                          </td>
                      </tr>
                  </xsl:for-each>

                  <xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='primary-medical-reviewer']">
                      <tr>
                          <td class='HwFlatCreditsColumn1'>Primary Medical Reviewer</td>
                          <td>
                              <xsl:value-of select='text()'/>
                          </td>
                      </tr>
                  </xsl:for-each>
                  <xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='secondary-medical-reviewer']">
                      <tr>
                          <td class='HwFlatCreditsColumn1'>Specialist Medical Reviewer</td>
                          <td>
                              <xsl:value-of select='text()'/>
                          </td>
                      </tr>
                  </xsl:for-each>

              </table>
          </xsl:if>
        <hr class='HwFlatReferenceHR' />
        <div class='HwFlatReferencesContent'>
          <div class='HwFlatReferencesTitle'>References</div>
          <div class='HwFlatCitationsTitle'>Citations</div>
          <div class='HwFlatCitationsContent'>
              <!-- temporary until credits, etc. come in xml docs -->
              <xsl:if test="count(/hw.doc/doc.sections/doc.section/section.std[@type='References']/blockquote/ol) > 0">
                  <xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='References']/blockquote/ol"/>
              </xsl:if>
          </div>
        </div>
      </div>
    </div>

  </xsl:template>
  
</xsl:stylesheet>

