<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="html"/>
	<xsl:template match="HEALTHWISE-COPYRIGHT">
		<p><xsl:value-of select="."/></p>
	</xsl:template>
</xsl:stylesheet>
