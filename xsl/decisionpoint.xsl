<?xml version="1.0"?>
<!--
================================================================================
Purpose: Templates for the new decision points
================================================================================
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:include href="flat_decisionpoint.xsl"/>
	<xsl:variable name="gvWarningAltText" select="'Warning: Incomplete Items'"/>
	<xsl:variable name="gvSummaryYourDecisionHeading" select="'Your decision'"/>
	<xsl:variable name="gvSummaryNextStepsHeading" select="'Next Steps'"/>
	<xsl:variable name="gvSummaryLeaningHeading" select='"Which way you&apos;re leaning"'/>
	<xsl:variable name="gvSummaryHowSureHeading" select="'How sure you are'"/>
	<xsl:variable name="gvSummaryCommentsHeading" select="'Your comments'"/>
	<xsl:variable name="gvSummaryKnowledgeHeading" select="'Your knowledge of the facts'"/>
	<xsl:variable name="gvSummaryConceptsUnderstoodHeading" select="'Key concepts that you understood'"/>
	<xsl:variable name="gvSummaryConceptsReviewHeading" select="'Key concepts that may need review'"/>
	<xsl:variable name="gvSummaryReadyToActHeading" select="'Getting ready to act'"/>
	<xsl:variable name="gvSummaryPatientChoicesHeading" select="'Patient Choices'"/>
	<xsl:variable name="gvSummaryMattersToYouHeading" select="'What matters to you'"/>
	<xsl:variable name="gvAccessibilityLinkText" select="'Turn on Accessibility Mode'"/>
	<xsl:variable name="gvAccessibilityLinkAltText" select="'Toggles between the text-only and interactive version of this Decision Point'"/>
	<!-- to be moved to static term dictionary for 8.3 -->
	<xsl:variable name="gvSectionTabGetTheFacts" select="'Get the Facts'"/>
	<xsl:variable name="gvSectionTabCompareOptions" select="'Compare Options'"/>
	<xsl:variable name="gvSectionTabYourFeelings" select="'Your Feelings'"/>
	<xsl:variable name="gvSectionTabYourDecision" select="'Your Decision'"/>
	<xsl:variable name="gvSectionTabQuizYourself" select="'Quiz Yourself'"/>
	<xsl:variable name="gvSectionTabYourSummary" select="'Your Summary'"/>
	<!--
================================================================================
 Structural Named Template
================================================================================
-->
	<xsl:template name="GetSectionName">
		<xsl:param name="idx"/>
		<xsl:choose>
			<xsl:when test="$idx = '1'">
				<xsl:value-of select="$gvSectionTabGetTheFacts"/>
			</xsl:when>
			<xsl:when test="$idx = '2'">
				<xsl:value-of select="$gvSectionTabCompareOptions"/>
			</xsl:when>
			<xsl:when test="$idx = '3'">
				<xsl:value-of select="$gvSectionTabYourFeelings"/>
			</xsl:when>
			<xsl:when test="$idx = '4'">
				<xsl:value-of select="$gvSectionTabYourDecision"/>
			</xsl:when>
			<xsl:when test="$idx = '5'">
				<xsl:value-of select="$gvSectionTabQuizYourself"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="doc.section" mode="DPTabCell">
		<xsl:variable name="index" select="count(preceding-sibling::*[starts-with(section.std/@type, 'Dp.')])+1"/>
		<xsl:variable name="name" select="substring-after(section.std/@type,'Dp.')"/>
		<xsl:variable name="title">
			<xsl:call-template name="GetSectionName">
				<xsl:with-param name="idx">
					<xsl:value-of select="$index"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<td>
			<div id="Hw{$name}Tab" class="HwSectionTab HwSectionTabLink">
				<div>
					<h3 class="HwSectionTabNumber">
						<a class="HwSectionTabLink" rel="Hw{$name}Tab" href="javascript:void(0);">
							<xsl:value-of select="$index"/>
						</a>
					</h3>
					<p>
						<xsl:call-template name="DPInsertBR">
							<xsl:with-param name="string" select="$title"/>
						</xsl:call-template>
					</p>
				</div>
			</div>
		</td>
	</xsl:template>
	<xsl:template name="DPInsertBR">
		<xsl:param name="string"/>
		<xsl:choose>
			<xsl:when test="contains(substring-after($string, ' '), ' ')">
				<xsl:value-of select="substring-before($string, ' ')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="substring-before(substring-after($string, ' '), ' ')"/>
				<br/>
				<xsl:value-of select="substring-after(substring-after($string, ' '), ' ')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring-before($string, ' ')"/>
				<br/>
				<xsl:value-of select="substring-after($string, ' ')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- This is the layout that is utilized for all generated HTML pages for medical content.  -->
	<xsl:template name="DecisionPointContent">
		<div id="HwDpPrinter" class="HwDpPrinter">
			<xsl:text> </xsl:text>
		</div>
		<p class="HwDecisionPointDisclaimer">
      You may want to have a say in this decision, or you may simply want to follow your doctor's recommendation. Either way, this information will help you understand what your choices are so that you can talk to your doctor about them.
    </p>
		<div class="HwDpTextOnlyLink">
			<a href="accessibility.html" title="{$gvAccessibilityLinkAltText}">
				<xsl:value-of select="$gvAccessibilityLinkText"/>
			</a>
		</div>
		<div id="HwDecisionPoint">
			<div class="HwDecisionPointHeadline">
				<p>
					<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.title"/>
				</p>
			</div>
			<div id="HwDecisionPointBody">
				<div id="HwSections">
					<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<xsl:apply-templates mode="DPTabCell" select="/hw.doc/doc.sections/doc.section[starts-with(section.std/@type, 'Dp.')]"/>
							<td>
								<div id="HwYourSummaryTab" class="HwSectionTab HwSectionTabLink">
									<div>
										<h3 class="HwSectionTabNumber">
											<a class="HwSectionTabLink" rel="HwYourSummaryTab" href="javascript:void(0);">6</a>
										</h3>
										<p>
                      Your<br/>
                      Summary
                    </p>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div id="HwGetTheFacts" class="HwTabSection">
					<xsl:call-template name="StartSection">
						<xsl:with-param name="pSectionName">
							<xsl:value-of select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.GetTheFacts']/../section.meta-data/meta-data.title"/>
						</xsl:with-param>
					</xsl:call-template>
					<div class="HwDecisionPointContent">
						<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.GetTheFacts']/*"/>
					</div>
				</div>
				<div id="HwCompareOptions" class="HwTabSection">
					<xsl:call-template name="StartSection">
						<xsl:with-param name="pSectionName">
							<xsl:value-of select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.CompareOptions']/../section.meta-data/meta-data.title"/>
						</xsl:with-param>
					</xsl:call-template>
					<div class="HwDecisionPointContent">
						<xsl:apply-templates select="/hw.doc/doc.sections/doc.section[section.std/@type='Dp.CompareOptions']" mode="CompareOptions"/>
					</div>
				</div>
				<div id="HwYourFeelings" class="HwTabSection">
					<xsl:call-template name="StartSection">
						<xsl:with-param name="pSectionName">
							<xsl:value-of select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.YourFeelings']/../section.meta-data/meta-data.title"/>
						</xsl:with-param>
					</xsl:call-template>
					<div class="HwDecisionPointContent">
						<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.YourFeelings']/*"/>
					</div>
				</div>
				<div id="HwYourDecision" class="HwTabSection">
					<xsl:call-template name="StartSection">
						<xsl:with-param name="pSectionName">
							<xsl:value-of select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.YourDecision']/../section.meta-data/meta-data.title"/>
						</xsl:with-param>
					</xsl:call-template>
					<div class="HwDecisionPointContent">
						<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.YourDecision']/*"/>
					</div>
				</div>
				<div id="HwQuizYourself" class="HwTabSection">
					<xsl:call-template name="StartSection">
						<xsl:with-param name="pSectionName">
							<xsl:value-of select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.QuizYourself']/../section.meta-data/meta-data.title"/>
						</xsl:with-param>
					</xsl:call-template>
					<div class="HwDecisionPointContent">
						<div id="HwQuizQuestionsWrapper">
							<div id="HwQuizQuestionsBorder">
								<div id="HwQuizQuestions">
									<!--<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.QuizYourself']" mode="QuizYourself"/>-->
									<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.QuizYourself']/*"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="HwYourSummary" class="HwTabSection">
					<xsl:call-template name="StartSection">
						<xsl:with-param name="pSectionName">
							<xsl:value-of select="'Your Summary'"/>
						</xsl:with-param>
					</xsl:call-template>
					<div class="HwDecisionPointContent">
						<xsl:call-template name="MakeYourSummarySection"/>
					</div>
				</div>
			</div>
			<div id="HwNavigationFooter">
				<div class="HwNavigationPrevious">
					<a class="HwNavigationPreviousLink" href="javascript:void(0);">
            &lt;
            Previous
          </a>
				</div>
				<div class="HwNavigationNext">
					<a class="HwNavigationNextLink" href="javascript:void(0);">Next &gt;</a>
				</div>
				<div class="HwDpPrintSummary">
					<a class="HwDpPrintSummaryLink" href="javascript:void(0);">Print Summary</a>
				</div>
			</div>
			<div id="HwReferences">
				<xsl:call-template name="References"/>
			</div>
		</div>
		<xsl:call-template name="HwFlatDPLayout">
			<xsl:with-param name="pRoot" select="'../..'"/>
		</xsl:call-template>
		<script type="text/javascript">
      var swfLocation = '/kbase/ssi/inc/control/dp/print.swf';
      var swfQuality = 'best';
      var alt = '';
      var width = '100%';
      var height = '100%';
      var so = new SWFObject(swfLocation, "HwPrinter", width, height, "8", "#000000");
      so.addParam("allowScriptAccess", "always");
      so.addParam("quality", swfQuality);
      so.addParam("bgcolor", "#ffffff");
      so.addParam("salign", "t");
      so.addParam("alt", alt);
      so.addParam("wmode", "transparent");
      so.addVariable("css", "/kbase/ssi/inc/style/dp/flash-print-styles.css");
      so.write("HwDpPrinter");
    </script>
	</xsl:template>
	<!--
================================================================================
 DP Templates
================================================================================
-->
	<xsl:template name="References">
		<!--TODO: Add Reference/Credits here-->
		<div class="HwReferenceContainer">
			<h3 class="HwReferenceBackgroundHeader">
				<a href="#">
					<span class="HwReferenceLabel">Credits and references</span>
				</a>
			</h3>
			<div class="HwReferenceContent">
				<div class="HwCreditsTitle">Credits</div>
				<!-- temporary until credits come in docs -->
				<xsl:if test="count(/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='author-name']) > 0">
					<table class="HwDpCredits">
						<xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='author-name']">
							<tr>
								<td class="HwCreditsColumn1">Author</td>
								<td>
									<xsl:value-of select="."/>
								</td>
							</tr>
						</xsl:for-each>
						<xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='editor-name']">
							<tr>
								<td class="HwCreditsColumn1">Editor</td>
								<td>
									<xsl:value-of select="."/>
								</td>
							</tr>
						</xsl:for-each>
						<xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='associate-editor-name']">
							<tr>
								<td class="HwCreditsColumn1">Associate Editor</td>
								<td>
									<xsl:value-of select="."/>
								</td>
							</tr>
						</xsl:for-each>
						<xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='primary-medical-reviewer']">
							<tr>
								<td class="HwCreditsColumn1">Primary Medical Reviewer</td>
								<td>
									<xsl:value-of select="."/>
								</td>
							</tr>
						</xsl:for-each>
						<xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person[@role='secondary-medical-reviewer']">
							<tr>
								<td class="HwCreditsColumn1">Specialist Medical Reviewer</td>
								<td>
									<xsl:value-of select="."/>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:if>
				<hr class="HwReferenceHR"/>
				<div class="HwReferencesContent">
					<div class="HwReferencesTitle">References</div>
					<div class="HwCitationsTitle">Citations</div>
					<!-- temporary until credits, etc. come in xml docs -->
					<xsl:if test="count(/hw.doc/doc.sections/doc.section/section.std[@type='References']/blockquote/ol) > 0">
						<div class="HwCitationsContent">
							<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='References']/blockquote/ol"/>
						</div>
					</xsl:if>
				</div>
			</div>
		</div>
	</xsl:template>
	<!--
================================================================================
 DP Section Templates
================================================================================
-->
	<xsl:template name="StartSection">
		<xsl:param name="pSectionName"/>
		<a class="HwFocusStop" href="javascript:void(0);">
			<xsl:text> </xsl:text>
					</a>
		<p class="HwSectionTitle">
			<xsl:value-of select="$pSectionName"/>
		</p>
		<div class="HwSectionSeperator">
			<xsl:text> </xsl:text>
		</div>
	</xsl:template>
	<xsl:template match="/hw.doc/doc.sections/doc.section" mode="CompareOptions">
		<div id="HwComparisonWrapper">
			<div id="HwComparisonBorder">
				<div id="HwComparison">
					<div id="HwComparisonTable">
						<div class="HwComparisonHeading">
							<div class="HwComparisonControlContainer">
								<div class="HwComparisonHeadingTitle">
									<p>Compare</p>
								</div>
								<div class="HwSelectComparisonOption">
									<select id="HwCompareOptionsA">
										<option value="Select">Select One</option>
										<xsl:for-each select="//div[@class='HwComparisonOption']">
											<option>
												<xsl:attribute name="value"><xsl:value-of select="normalize-space(span[@class='HwOptionName'])"/></xsl:attribute>
												<xsl:value-of select="normalize-space(span[@class='HwOptionName'])"/>
											</option>
										</xsl:for-each>
									</select>
								</div>
								<div class="HwSelectComparisonOption">
									<select id="HwCompareOptionsB">
										<option value="Select">Select One</option>
										<xsl:for-each select="//div[@class='HwComparisonOption']">
											<option>
												<xsl:attribute name="value"><xsl:value-of select="normalize-space(span[@class='HwOptionName'])"/></xsl:attribute>
												<xsl:value-of select="normalize-space(span[@class='HwOptionName'])"/>
											</option>
										</xsl:for-each>
									</select>
								</div>
							</div>
						</div>
						<!--End ComparisonHeading-->
						<table class="HwComparisonQuestions">
							<tr>
								<th>
									<xsl:text> </xsl:text>
																	</th>
								<th>
									<xsl:text> </xsl:text>
								</th>
								<th>
									 <xsl:text> </xsl:text>
								</th>
							</tr>
							<tr class="HwComparisonQuestion">
								<td class="HwComparisonQuestionText">
									<p>What is usually involved?</p>
								</td>
								<td class="HwComparisonItem">
									<p>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
									</p>
								</td>
								<td class="HwComparisonItem">
									<p>
										<xsl:text> </xsl:text>
									</p>
								</td>
							</tr>
							<tr class="HwComparisonQuestion">
								<td class="HwComparisonQuestionText">
									<p>What are the benefits?</p>
								</td>
								<td class="HwComparisonItem">
									<p>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
									</p>
								</td>
								<td class="HwComparisonItem">
									<p>
										<xsl:text> </xsl:text>
									</p>
								</td>
							</tr>
							<tr class="HwComparisonQuestion">
								<td class="HwComparisonQuestionText">
									<p>What are the risks and side effects?</p>
								</td>
								<td class="HwComparisonItem">
									<p>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
										<br/>
									</p>
								</td>
								<td class="HwComparisonItem">
									<p>
										<xsl:text> </xsl:text>
									</p>
								</td>
							</tr>
						</table>
						<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.CompareOptions']/div[@id='HwComparisonOptions']"/>
						<div id="HwPersonalStoriesHeadlineWrapper">
							<div id="HwPersonalStoriesHeadlineBorder">
								<div id="HwPersonalStoriesHeadline">
									<p class="HwStoriesTitle">Personal Stories</p>
									<p>
                    Are you interested in what others decided to do? Many people have faced this decision. These personal stories may help you decide.
                  </p>
									<div class="HwPersonalStoriesWindowLink">
										<a href="javascript:void(0);">Read More</a>
									</div>
								</div>
							</div>
						</div>
						<!-- End PersonalStoriesHeadlineWrapper-->
					</div>
					<!-- End ComparisonTable-->
				</div>
				<!-- End Comparison -->
			</div>
			<!-- End ComparisonBorder -->
		</div>
		<!-- End ComparisonWrapper -->
		<xsl:call-template name="PersonalStories"/>
		<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='Dp.CompareOptions']/div[@class='HwNeedMoreInformation']"/>
		<!-- End DecisionPointContent -->
		<div class="HwPSOverlay" id="HwPSOverlay">
			<xsl:text> </xsl:text>
		</div>
	</xsl:template>
	<xsl:template match="div[@class='HwOptionInvolved']/h4|div[@class='HwOptionBenefits']/h4|div[@class='HwOptionRisks']/h4">
		<!-- suppress these -->
	</xsl:template>
	<xsl:template match="div[@class='HwNeedMoreInformation']">
		<xsl:element name="{name()}">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="link.content[@document-type = 'Citation']">
		<sub class="HwSub">
			<a>
				<xsl:attribute name="href"><xsl:value-of select="@document-href"/></xsl:attribute>
				<xsl:number value="text()"/>
			</a>
		</sub>
	</xsl:template>
	<xsl:template name="PersonalStories">
		<div id="HwPersonalStories">
			<div id="HwPersonalStoriesWindow">
				<div id="HwPersonalStoriesContentWrapper">
					<div id="HwPersonalStoriesBorder">
						<div id="HwPersonalStoriesHeader">
							<div class="HwActions">
								<a class="HwCloseLink" href="javascript:void(0);">close</a>
							</div>
							<!-- End Actions -->
							<div class="HwPersonalStoriesContent" id="HwPersonalStoriesContentArea">
								<xsl:apply-templates select="/hw.doc/doc.sections/doc.section/section.std[@type='PersonalStories']/div[@class='HwAboutPersonalStories']"/>
								<xsl:text> </xsl:text>
							</div>
							<!-- End PersonalStoriesContent-->
						</div>
						<!-- End PersonalStoriesHeader -->
						<div class="HwStories" id="HwStories">
							<div class="HwPersonalStoriesContent">
								<xsl:for-each select="/hw.doc/doc.sections/doc.section/section.std[@type='PersonalStories']/div[@id='HwPersonalStories']/div[@class='HwPersonalStory']">
									<xsl:choose>
										<xsl:when test="position() mod 2 = 1">
											<div class="HwPersonalStory HwPersonalStoryOdd">
												<xsl:apply-templates select="div[@class='HwPersonalStoryBody']"/>
												<xsl:apply-templates select="div[@class='HwPersonalStoryIdentity']"/>
											</div>
										</xsl:when>
										<xsl:otherwise>
											<div class="HwPersonalStory HwPersonalStoryEven">
												<xsl:apply-templates select="div[@class='HwPersonalStoryBody']"/>
												<xsl:apply-templates select="div[@class='HwPersonalStoryIdentity']"/>
											</div>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<xsl:text> </xsl:text>
							</div>
						</div>
						<div class="HwStoriesFooter">
							<xsl:text> </xsl:text>
						</div>
					</div>
					<!-- End HwPersonalStoriesBorder -->
				</div>
				<!-- End HwPersonalStoriesContentWrapper -->
			</div>
			<!-- End HwPersonalStoriesWindow-->
		</div>
		<!-- End HwPersonalStories -->
	</xsl:template>
	<!--
================================================================================
 DP Accordion Templates
================================================================================
-->
	<xsl:template match="div[@id='HwFAQs']">
		<xsl:element name="{name()}">
			<xsl:copy-of select="@*"/>
			<div class="HwFAQsTitle">
        FAQs
      </div>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="h4[@class = 'HwFAQQuestion']">
		<xsl:variable name="vQuestion" select="."/>
		<h3 class="HwFAQBackgroundHeader">
			<a href="#">
				<span class="HwFAQQuestion">
					<xsl:value-of select="$vQuestion"/>
				</span>
			</a>
		</h3>
	</xsl:template>
	<xsl:template match="div[@class = 'HwFAQAnswer']">
		<div class="HwFAQContent">
			<xsl:element name="{name()}">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates/>
			</xsl:element>
		</div>
	</xsl:template>
	<xsl:template match="p" mode="FAQAnswer">
		<xsl:element name="{name()}">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!--
================================================================================
 Slider Templates
================================================================================
-->
	<xsl:template match="div[@class = 'HwOptionsHeader']">
		<div class="HwSliderBackground HwCornerAll">
			<xsl:apply-templates/>
			<div class="HwHelperStopFloat">
				<xsl:text> </xsl:text>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="div[contains(@class,'HwMultiOptionSlider')]">
		<xsl:element name="{local-name(.)}">
			<xsl:copy-of select="@*"/>
			<xsl:variable name="vIsRequired">
				<xsl:if test="contains(@class,'HwNotRequired')">
					<xsl:text>HwNotRequired</xsl:text>
				</xsl:if>
			</xsl:variable>
			<div class="HwSliderBackground HwCornerAll">
				<xsl:apply-templates select="div[@class = 'HwSliderText']"/>
				<div class="HwSliderContainer">
					<div>
						<xsl:attribute name="class"><xsl:text>HwSlider</xsl:text><xsl:if test="$vIsRequired != ''"><xsl:text> optional</xsl:text></xsl:if><xsl:if test="ancestor::div[@class='HwSliderQuestion']"><xsl:text> SliderQuestion</xsl:text></xsl:if></xsl:attribute>
						<xsl:text> </xsl:text>
					</div>
				</div>
				<xsl:apply-templates select="div[@class = 'HwSliderLabel']"/>
				<div class="HwHelperStopFloat">
					<xsl:text> </xsl:text>
				</div>
			</div>
		</xsl:element>
	</xsl:template>
	<xsl:template match="div[contains(@class,'HwMultiOptionSliderLeft')]">
		<xsl:element name="{local-name(.)}">
			<xsl:copy-of select="@*"/>
			<xsl:variable name="vIsRequired">
				<xsl:if test="contains(@class,'HwNotRequired')">
					<xsl:text>HwNotRequired</xsl:text>
				</xsl:if>
			</xsl:variable>
			<div class="HwSliderBackgroundSolid HwCornerAll">
				<xsl:apply-templates select="div[@class = 'HwSliderText']"/>
				<div class="HwSliderContainer">
					<div>
						<xsl:attribute name="class"><xsl:text>HwSliderLeft</xsl:text><xsl:if test="$vIsRequired != ''"><xsl:text> optional</xsl:text></xsl:if><xsl:if test="ancestor::div[@class='HwSliderQuestion']"><xsl:text> SliderQuestion</xsl:text></xsl:if></xsl:attribute>
						<xsl:text> </xsl:text>
					</div>
				</div>
				<xsl:apply-templates select="div[@class = 'HwSliderLabel']"/>
				<div class="HwHelperStopFloat">
					<xsl:text> </xsl:text>
				</div>
			</div>
		</xsl:element>
	</xsl:template>
	<xsl:template match="div[@class = 'HwSliderText'] | div[@class = 'HwSliderLabel']">
		<xsl:element name="{local-name(.)}">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
			<div class="HwHelperStopFloat">
				<xsl:text> </xsl:text>
			</div>
		</xsl:element>
	</xsl:template>
	<xsl:template match="p[@class = 'HwOptionTextRight'] | p[@class = 'HwOptionTextLeft']">
		<xsl:element name="{local-name(.)}">
			<xsl:variable name="className" select="@class"/>
			<xsl:attribute name="class"><xsl:text>HwOptionText </xsl:text><xsl:value-of select="$className"/></xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!--
================================================================================
 DP Quiz Templates
================================================================================
-->
	<xsl:template match='div[@id = "HwCheckYourFacts"]/p'>
		<div class="HwQuizSubtitleTop">
			<p class="HwQuizSubtitleText">
				<xsl:value-of select="text()"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template match='div[@id = "HwCertainty"]/p | div[@id = "HwYourChoices"]/p'>
		<div class="HwQuizSubtitle">
			<p class="HwQuizSubtitleText">
				<xsl:value-of select="text()"/>
			</p>
		</div>
	</xsl:template>
	<xsl:template match="div[contains(@class,'HwMultipleChoiceQuestion')]">
		<div>
			<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
			<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
			<xsl:apply-templates/>
		</div>
	</xsl:template>
	<xsl:template match="div[contains(@class,'HwAnswers')]">
		<div class="HwAnswers">
			<ul>
				<xsl:for-each select="ul/li">
					<li class="HwAnswer">
						<span class="HwRadioButton">
							<xsl:apply-templates select="input"/>
						</span>
						<xsl:for-each select="span">
							<xsl:apply-templates select="."/>
						</xsl:for-each>
						<xsl:if test="@class= 'HwAnswer HwCorrectAnswer'">
							<span class="HwCorrectAnswer">
								<xsl:text> </xsl:text>
							</span>
						</xsl:if>
					</li>
				</xsl:for-each>
			</ul>
			<div class="HwClear">
				<xsl:text> </xsl:text>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="span[@class='HwAnswerText']">
		<span class="HwAnswerTextWrapper">
			<xsl:element name="{local-name(.)}">
				<xsl:copy-of select="@*"/>
				<a class="HwTabStopLink" href="javascript:void(0);" rel=".HwRadioButton" onclick="return false;">
					<xsl:apply-templates/>
				</a>
			</xsl:element>
		</span>
	</xsl:template>
	<xsl:template match="div[@class= 'HwBooleanQuestion']">
		<div class="HwBooleanQuestion">
			<xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
			<span class="HwOrderedNumber">
				<xsl:value-of select="count(../preceding-sibling::div[@class='HwQuizQuestion']) + 1"/>.
      </span>
			<xsl:for-each select="input">
				<xsl:choose>
					<xsl:when test="@value = 'Yes'">
						<span class="HwYesButton">
							<xsl:apply-templates select="."/>
						</span>
					</xsl:when>
					<xsl:when test="@value = 'No'">
						<span class="HwNoButton">
							<xsl:apply-templates select="."/>
						</span>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
			<xsl:apply-templates select="p"/>
		</div>
	</xsl:template>
	<xsl:template match="p[@class='HwQuestionText' and parent::div[@class!='HwBooleanQuestion']]">
		<span class="HwOrderedNumber">
			<xsl:value-of select="count(../../preceding-sibling::div[@class='HwQuizQuestion']) + 1"/>.
    </span>
		<p class="HwQuestionText">
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<!--
================================================================================
 DP Summary Templates
================================================================================
-->
	<xsl:template name="MakeYourSummarySection">
		<p>
      Here's a record of your answers. You can use it to talk with your doctor or loved ones about your decision.
    </p>
		<div id="HwSummary">
			<div class="HwSummaryContainer" id="HwYourDecisionSummary">
				<h3 class="HwSummaryBackgroundHeader">
					<a href="#">
						<xsl:value-of select="$gvSummaryYourDecisionHeading"/>
						<img alt="{$gvWarningAltText}" class="HwAccordionWarning">
							<xsl:attribute name="src"><xsl:text>/kbase/ssi/inc/style/dp/images/TabIncompleteIndicatorSmall.png</xsl:text></xsl:attribute>
						</img>
					</a>
				</h3>
				<div class="HwSummarySection">
					<div class="HwBackgroundBox">
						<p class="HwSummarySectionHeading">
							<xsl:value-of select="$gvSummaryNextStepsHeading"/>
						</p>
					</div>
					<div class="HwSummarySectionHeading" id="HwNextStepsSummary">
						<xsl:text> </xsl:text>
					</div>
					<div class="HwBackgroundBox">
						<p class="HwSummarySectionHeading">
							<xsl:value-of select="$gvSummaryLeaningHeading"/>
						</p>
					</div>
					<div class="HwSummarySectionSubHeading" id="HwLeaningSummary">
						<xsl:text> </xsl:text>
					</div>
					<div class="HwBackgroundBox">
						<p class="HwSummarySectionHeading">
							<xsl:value-of select="$gvSummaryHowSureHeading"/>
						</p>
					</div>
					<div class="HwSummarySectionSubHeading" id="HwHowSureSummary">
						<xsl:text> </xsl:text>
					</div>
					<div class="HwBackgroundBox">
						<p class="HwSummarySectionHeading">
							<xsl:value-of select="$gvSummaryCommentsHeading"/>
						</p>
					</div>
					<p class="HwSummarySectionHeading" id="HwYourCommentsSummary">
						<xsl:text> </xsl:text>
					</p>
				</div>
			</div>
			<div class="HwSummaryContainer">
				<h3 class="HwSummaryBackgroundHeader">
					<a href="#">
						<xsl:value-of select="$gvSummaryKnowledgeHeading"/>
						<img alt="{$gvWarningAltText}" class="HwAccordionWarning">
							<xsl:attribute name="src"><xsl:text>/kbase/ssi/inc/style/dp/images/TabIncompleteIndicatorSmall.png</xsl:text></xsl:attribute>
						</img>
					</a>
				</h3>
				<div class="HwSummarySection" id="HwKeyConceptsSummary">
					<div class="HwBackgroundBox">
						<p class="HwSummarySectionHeading">
							<xsl:value-of select="$gvSummaryConceptsUnderstoodHeading"/>
						</p>
					</div>
					<div id="HwSummaryFactCheckCorrect">
						<xsl:text> </xsl:text>
					</div>
					<div class="HwBackgroundBox">
						<p class="HwSummarySectionHeading">
							<xsl:value-of select="$gvSummaryConceptsReviewHeading"/>
						</p>
					</div>
					<div id="HwSummaryFactCheckIncorrect">
						<xsl:text> </xsl:text>
					</div>
				</div>
			</div>
			<div class="HwSummaryContainer">
				<h3 class="HwSummaryBackgroundHeader">
					<a href="#">
						<xsl:value-of select="$gvSummaryReadyToActHeading"/>
						<img alt="{$gvWarningAltText}" class="HwAccordionWarning">
							<xsl:attribute name="src"><xsl:text>/kbase/ssi/inc/style/dp/images/TabIncompleteIndicatorSmall.png</xsl:text></xsl:attribute>
						</img>
					</a>
				</h3>
				<div class="HwSummarySection" id="HwReadyToActSummary">
					<div class="HwBackgroundBox">
						<p class="HwSummarySectionHeading">
							<xsl:value-of select="$gvSummaryPatientChoicesHeading"/>
						</p>
					</div>
					<div id="HwSummaryPatientChoices">
						<xsl:text> </xsl:text>
					</div>
				</div>
			</div>
			<div class="HwSummaryContainer">
				<h3 class="HwSummaryBackgroundHeader">
					<a href="#">
						<xsl:value-of select="$gvSummaryMattersToYouHeading"/>
						<img alt="{$gvWarningAltText}" class="HwAccordionWarning">
							<xsl:attribute name="src"><xsl:text>/kbase/ssi/inc/style/dp/images/TabIncompleteIndicatorSmall.png</xsl:text></xsl:attribute>
						</img>
					</a>
				</h3>
				<div class="HwSummarySection" id="HwMattersToYouSummary">
					<xsl:text> </xsl:text>
				</div>
			</div>
		</div>
	</xsl:template>
	<!--
================================================================================
 Section Utils Templates
================================================================================
-->
	<xsl:template name="SectionUtils_getNavstackEntrySectionId">
		<xsl:param name="pSectionName"/>
		<xsl:if test="$pSectionName = ''">
			<xsl:message terminate="yes">Call to SectionUtils_getNavstackEntrySectionId without required parameter pSectionName null.</xsl:message>
		</xsl:if>
		<xsl:for-each select="/hw.doc/doc.prebuilt/prebuilt.navstack/navstack.entries/navstack.entry">
			<xsl:variable name="vSectionTitle" select="text()"/>
			<xsl:if test="$vSectionTitle = $pSectionName">
				<xsl:value-of select="@section-href"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
