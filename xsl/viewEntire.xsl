<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="no"/>
<!-- <xsl:output method="xml" omit-xml-declaration="yes" indent="no" /> -->

<xsl:include href="sharedTemplates.xsl"/>

<!-- select overall layout of document -->
<xsl:template name="PageLayout">
	<!-- display the document title in the browser title bar -->
	<xsl:element name="title">
		<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.title"/>
	</xsl:element>
	
	<xsl:call-template name="BodyLayoutA"/>
	
</xsl:template>

<!-- ***
Body for primary documents
*** -->
<xsl:template name="BodyLayoutA">
	<a name="_top"/>
	<table border="0" cellpadding="0" height="100%" width="100%">
		<tr valign="top">
			<td>
				<table border="0" width="100%">
					<tr>
						<td>			
						<!-- Do we need to label the source of 3rd party content? -->
							<xsl:choose>
							<!-- NORD content. -->
								<xsl:when test="$gvDocType = 'Nord'">
									<h4>National Organization for Rare Disorders, Inc.</h4>
								</xsl:when>
							<!-- SHC content. -->
								<xsl:when test="$gvDocType = 'Shc'">
									<h4>Self Help Clearinghouse</h4>
								</xsl:when>
							</xsl:choose>

						<!-- Output the document title. -->
							<h2><xsl:apply-templates select="/hw.doc/doc.meta-data/meta-data.title"/></h2>
													
						<!--  render all sections on one page -->
							
									<xsl:for-each select="doc.sections/section.include/doc.section|doc.sections/doc.section/section.std[not(@type = 'Credits')] | 
									doc.sections/section.include/doc.section|doc.sections/doc.section/section.emer | 
									doc.sections/section.include/doc.section|doc.sections/doc.section/section.cys | 
									doc.sections/section.include/doc.section|doc.sections/doc.section/section.qdoc"> <!-- * -->
									<!-- not(@type = 'References') and  -->
										
											<!-- if there is a section title that is different than the main doc title, show it -->
												<xsl:if test="ancestor-or-self::doc.section/section.meta-data/meta-data.title">
													<xsl:if test="ancestor-or-self::doc.section/section.meta-data/meta-data.title != /hw.doc/doc.meta-data/meta-data.title">
														<a name="{ancestor-or-self::doc.section/@hwid-content}">
														<h3><xsl:value-of select="ancestor-or-self::doc.section/section.meta-data/meta-data.title"/></h3>
														</a>
													</xsl:if>
												</xsl:if>
											<!-- Process all of children of the current section. -->
												<xsl:apply-templates/>
									</xsl:for-each>

						<!-- Section Footer -->
							<xsl:call-template name="SectionFooter"/>
					
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</xsl:template>

    <!--
    ====================================================================
    Section Footer
        Generate Previous Section, Top, and Next Section links at
        bottom of section, URAC table, copyright, and disclaimer.
    ====================================================================
    -->
    <xsl:template name="SectionFooter">
        <table border="0" cellpadding="5" align="center" width="90%">

            <xsl:call-template name="URAC"/>

            <tr>
                <td colspan="3">
                <br/>

                <!-- Do not output the HW copyright notice if this is 3rd party content. -->
                <xsl:if test="/hw.doc/@hwContent = 'true' and /hw.doc/@type !='Support'">
                    <xsl:call-template name="Copyright"/>

						 <!-- Do not output the HW disclaimer if this is a profile. -->
						 <xsl:if test="$gvDocType != 'HwProfile'">
							  <xsl:apply-templates select="/hw.doc/doc.prebuilt/prebuilt.disclaimer"/>
						 </xsl:if>
                </xsl:if>

                <br/>
                </td>
            </tr>
            <xsl:choose>
                <xsl:when test="$gvDocType = 'Definition' or
										  $gvDocType = 'MultiMedia' or
										  $gvDocType = 'CalcDoc' or
										  $gvDocType = 'QDoc'"/>
                <xsl:otherwise>
                    <tr>
                        <td align="center" colspan="3">
                            <xsl:call-template name="HWLogo"/>
                        </td>
                    </tr>
                </xsl:otherwise>
            </xsl:choose>
        </table>
    </xsl:template>

    <!--
    ====================================================================
    URAC
        Render the prebuilt URAC table.
    ====================================================================
    -->
    <xsl:template name="URAC">
        <xsl:choose>
            <xsl:when test="/hw.doc/doc.prebuilt/prebuilt.creditsfooter">
                <tr><td colspan="3"><hr/></td></tr>
                <!-- Output the prebuilt URAC table rows. -->
                <xsl:apply-templates select="/hw.doc/doc.prebuilt/prebuilt.creditsfooter/table/tr"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- instead of revision date just output a break -->
                <tr><td colspan="3"><br/></td></tr>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    
    <xsl:template name="HWLogo">
		<img src="/kbase/hwxml/media/interface/hwlogo.gif" border="0" alt="Click here to learn about Healthwise"/>
    </xsl:template>

    <xsl:template name="Copyright">
    	<p class="HWCopyright"><xsl:apply-templates select="document('../hwxml/docs/hwcopyright.xml')/HEALTHWISE-COPYRIGHT/node()"/></p>
    </xsl:template>


    <!--
    Default Element Template - Used to handle the majority of the XHTML elements
    that occur within Healthwise content.
    -->
    <xsl:template match="*">
        
		<xsl:copy>
            <xsl:for-each select="@*">
                <xsl:choose>
                    <xsl:when test="self::class">
                        <xsl:choose>
                            <!-- Keep these classes as they will be handled via CSS or applications. -->
                            <xsl:when test=". = 'Note'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Red'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Size1'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Size2'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Size3'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Size4'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Sizeminus'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Sizeplus'"><xsl:copy-of select="."/></xsl:when>
                            <xsl:when test=". = 'Small'"><xsl:copy-of select="."/></xsl:when>
                            <!-- Suppress these classes. -->
                            <xsl:when test=". = 'NoWebPrint'"/>
                            <xsl:otherwise>
                                <!-- Throw an error that an unknown class was encountered -->
                                <xsl:message terminate="yes">An element with an unknown class, "<xsl:value-of select="."/>", was encountered.</xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="."/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>

            <xsl:apply-templates/>
        </xsl:copy>

    </xsl:template>
    
    <xsl:template match="div">
		<xsl:choose>
		<!-- special processing for ConditionCenter a.k.a. "Learning Centers" topics, which contain a list of relevant topics -->
			<xsl:when test="@class = 'ConditionCenterTitle'">
				<h4><xsl:value-of select="."/></h4>
				<ul>
					<xsl:for-each select="following-sibling::div[@class='ConditionCenterItem']">
						<li><xsl:apply-templates/></li>
					</xsl:for-each>
				</ul>
			</xsl:when>
			<xsl:when test="@class = 'ConditionCenterItem'"/>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


<!--
================================================================================
 Section 3.2 : HW Element Templates
================================================================================
-->

    <xsl:template match="/hw.doc">
		<xsl:call-template name="PageLayout"/>
    </xsl:template>


    <!-- Suppress meta-data content -->
    <xsl:template match="doc.meta-data|section.meta-data"/>
    
    
    <xsl:template match="meta-data.title">
        <xsl:apply-templates/>
    </xsl:template>
    
    
    <xsl:template match="prebuilt.disclaimer | prebuilt.creditsfooter">
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="section.std | section.emer | section.cys | section.qdoc">
        <xsl:apply-templates/>
    </xsl:template>


    <!-- Suppress QDoc results elements -->
    <xsl:template match="qdoc.results"/>


    <xsl:template match="block.drugs">
        <table border="1" cellpadding="5">
            <tr>
                <!-- Are there any brand names? -->
                <xsl:if test=".//drug.brand-names">
                    <th>Brand Name</th>
                </xsl:if>

                <!-- Are there any generic names? -->
                <xsl:if test=".//drug.generic-name">
                    <th>Generic Name</th>
                </xsl:if>

                <th>Chemical Name</th>

            </tr>

            <!-- Determine the number of columns in the drug table -->
            <xsl:variable name="vDrugCols">
                <xsl:choose>
                    <xsl:when test=".//drug.generic-name and .//drug.brand-names">3</xsl:when>
                    <xsl:when test=".//drug.generic-name">2</xsl:when>
                    <xsl:when test=".//drug.brand-names">2</xsl:when>
                    <xsl:otherwise>1</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:apply-templates>
                <xsl:with-param name="pDrugCols" select="$vDrugCols"/>
            </xsl:apply-templates>
        </table>
    </xsl:template>


    <xsl:template match="block.drug">
        <xsl:param name="pDrugCols"/>

        <tr>
            <xsl:choose>
                <xsl:when test="$pDrugCols = '3' and drug.generic-name and drug.brand-names">
                    <td><b><xsl:apply-templates select="drug.brand-names"/></b></td>
                    <td><b><xsl:apply-templates select="drug.generic-name"/></b></td>
                </xsl:when>

                <xsl:when test="$pDrugCols = '3' and drug.generic-name">
                    <td>&#160;</td>
                    <td><b><xsl:apply-templates select="drug.generic-name"/></b></td>
                </xsl:when>

                <xsl:when test="$pDrugCols = '3' and drug.brand-names">
                    <td><b><xsl:apply-templates select="drug.brand-names"/></b></td>
                    <td>&#160;</td>
                </xsl:when>

                <xsl:when test="$pDrugCols = '2' and drug.generic-name">
                    <td><b><xsl:apply-templates select="drug.generic-name"/></b></td>
                </xsl:when>

                <xsl:when test="$pDrugCols = '2' and drug.brand-names">
                    <td><b><xsl:apply-templates select="drug.brand-names"/></b></td>
                </xsl:when>
            </xsl:choose>

            <td><b><xsl:apply-templates select="drug.chemical-name"/></b></td>
        </tr>
    </xsl:template>


    <xsl:template match="drug.brand-name">
        <xsl:apply-templates/>
        <xsl:if test="position() != last() and count(../drug.brand-name) > 1">, </xsl:if>
    </xsl:template>


    <!-- Quiz questions are numbered. -->
    <xsl:template match="quiz.questions">
        <ol>
            <xsl:apply-templates/>
        </ol>
    </xsl:template>


    <xsl:template match="cys.chart-text"/>


    <xsl:template match="question.text">
        <xsl:choose>
            <xsl:when test="parent::cys.question">
                <tr>
                    <td>
                        <xsl:apply-templates/>
                    </td>
                </tr>
            </xsl:when>

            <xsl:when test="parent::severity.question">
                <tr>
                    <td>
                        <xsl:apply-templates/>
                    </td>
                </tr>
            </xsl:when>

            <xsl:when test="parent::quiz.question">
                <li>
                    <xsl:apply-templates/>
                </li>
            </xsl:when>

            <xsl:when test="parent::section.emer">
                <table border="0">
                    <tr>
                        <td valign="top">
                            <xsl:apply-templates/>
                        </td>
                    </tr>
                </table>
            </xsl:when>

            <xsl:otherwise>
                <!-- Throw an error that an unexpected condition was encountered -->
                <xsl:message terminate="yes">An unexpected condition was encountered processing element: <xsl:value-of select="local-name(.)"/>. Closest HWID: <xsl:value-of select="ancestor-or-self::*[@hwid-content]/@hwid-content"/>".</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- Quiz answers are lettered. -->
    <xsl:template match="question.answers">
        <ol type="a">
            <xsl:apply-templates/>
        </ol>
    </xsl:template>


    <!-- During document order processing, only need to output the answer text and not the explanation. -->
    <xsl:template match="question.answer">
            <xsl:apply-templates select="answer.text"/>
    </xsl:template>


    <xsl:template match="answer.text">
        <li>
                <xsl:apply-templates/>
        </li>
    </xsl:template>


    <xsl:template match="cys.questions">
        <table border="0">
            <xsl:apply-templates/>
        </table>
    </xsl:template>


    <!-- During document order processing, only need to output the question.text and not cys.chart-text or cys.when-to-see. -->
    <xsl:template match="cys.question">
        <xsl:apply-templates select="question.text"/>
    </xsl:template>


    <xsl:template match="cys.severity-groups">
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="cys.severity-group">
        <table border="2" cellpadding="5" cellspacing="0" width="80%">
            <!-- Set the correct colors for the table based on the severity. -->
            <xsl:choose>
                <xsl:when test="@severity = 's1'">
                    <xsl:attribute name="bordercolordark">DARKRED</xsl:attribute>
                    <xsl:attribute name="bordercolorlight">LIGHTCORAL</xsl:attribute>
                </xsl:when>

                <xsl:when test="@severity = 's2'">
                    <xsl:attribute name="bordercolordark">GOLD</xsl:attribute>
                    <xsl:attribute name="bordercolorlight">LIGHTYELLOW</xsl:attribute>
                </xsl:when>

                <xsl:when test="@severity = 's3'">
                    <xsl:attribute name="bordercolordark">GREEN</xsl:attribute>
                    <xsl:attribute name="bordercolorlight">LIGHTGREEN</xsl:attribute>
                </xsl:when>

                <xsl:when test="@severity = 's4'">
                    <xsl:attribute name="bordercolordark">BLACK</xsl:attribute>
                    <xsl:attribute name="bordercolorlight">C0C0C0</xsl:attribute>
                </xsl:when>

                <xsl:otherwise>
                    <xsl:message terminate="yes">Unexpected severity value while processing cys.severity-group: <xsl:value-of select="@severity"/>.</xsl:message>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:apply-templates/>
        </table>

        <br/>
    </xsl:template>


    <xsl:template match="severity-group.header">
        <tr>
            <td>
                <xsl:apply-templates/>
            </td>
        </tr>
    </xsl:template>


    <!-- During document order processing, only need to output the question.text and not cys.chart-text. -->
    <xsl:template match="severity.question">
        <xsl:apply-templates select="question.text"/>
    </xsl:template>


    <!-- open the popoffwindow for the cit-ref -->
    <xsl:template match="inline.cit-ref">
        <sup>
                <xsl:number level="any"/>
        </sup>
    </xsl:template>


    <xsl:template match="inline.emer-num">
        <span class="EmergencyNumber">
            <xsl:text> </xsl:text>
            	<xsl:value-of select="$emergencyNumber"/>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>
    
    
    <xsl:template match="inline.measurement">
		<xsl:choose>
			<xsl:when test="@unit2">
				<xsl:choose>
					<xsl:when test="@unit1 = 'F'">
						<xsl:value-of select="concat(@initval, ' degrees ', @unit1, ' (', @convval, ' degrees ', @unit2, ')')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat(@initval, ' ', @unit1, ' (', @convval, ' ', @unit2, ')')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(@initval, ' ', @unit1)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="link.content">
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="img"/>


	<xsl:template match="comment()"/>


	<xsl:template match="processing-instruction()"/>


</xsl:stylesheet>