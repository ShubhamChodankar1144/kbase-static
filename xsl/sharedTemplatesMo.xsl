<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- global parameters -->
	<xsl:param name="xmlFilePath"/>
	<xsl:param name="emergencyNumber" select="'911'"/>
	<xsl:param name="secId" select="'notPassed'"/><!-- Section hwid passed from pages within kbase -->
	<xsl:param name="sectionId" select="'notPassed'"/><!--Section hwid passed from search results -->
	<xsl:param name="wtsId" select="'notPassed'"/><!-- cys.when-to-see hwid passed from Symptom docs for When to See sections -->
	<xsl:param name="quizId" select="'notPassed'"/><!-- question.answer hwid passed from Actionset quizzes -->
	<xsl:param name="slideNum" select="'notPassed'"/><!-- slide number, passed from block.slideshow -->
	<xsl:param name="mghEnv" select="'notPassed'"/><!-- Environment droplet, dev, qa, or prod -->
	<xsl:param name="serverName" select="'notPassed'"/><!-- Server name -->
		
	<!-- global variables -->
	<xsl:variable name="gvRootElement" select="local-name(/*[1])"/>
	<xsl:variable name="gvDocType" select="/hw.doc/@type"/>
	<xsl:variable name="gvDocRank" select="/hw.doc/@rank"/>
	<xsl:variable name="gvDocLang" select="/hw.doc/@xml:lang"/>
	<xsl:variable name="gvDocHWID" select="/hw.doc/@hwid-content"/>
	<xsl:variable name="gvCopyrightYear" select="'1995-2008'"/><!-- Copyright year -->
	<xsl:variable name="gViewer" select="'/kbase/MomentumTopic.jhtml'"/><!-- JHTML XMLTransform viewer page -->
	<xsl:variable name="gViewEntire" select="'/kbase/entireTopic.jhtml'"/><!-- view entire topic JHTML page -->	

	<xsl:variable name="docGhCustom" select="document('../ssi/custom/ghCustom.xml')"/><!-- load  the GHC customizations mapping file -->
 
	<xsl:variable name="gvPassedSecId"><!-- which section to render -->	
		<xsl:choose>
			<xsl:when test="not($secId = 'notPassed')">
				<xsl:choose>
					<xsl:when test="/hw.doc/doc.sections//doc.section/@hwid-content = $secId">
						<xsl:value-of select="$secId"/>
					</xsl:when>
					<xsl:otherwise>notPassed</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="not($sectionId = 'notPassed')">
				<xsl:choose>
					<xsl:when test="/hw.doc/doc.sections//doc.section/@hwid-content = $sectionId">
						<xsl:value-of select="$sectionId"/>
					</xsl:when>
					<xsl:otherwise>notPassed</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>notPassed</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="gvSlideNumber"><!-- block.slideshow prev/next navigation -->
		<xsl:choose>
			<xsl:when test="$slideNum = 'notPassed'">1</xsl:when>
			<xsl:otherwise><xsl:value-of select="$slideNum"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<!-- key declarations -->
 	<xsl:key name="kGhCustom" match="Custom" use="Topics/Topic"/>
	<xsl:key name="kSection" match="doc.section" use="@hwid-content"/>
	
	<!-- DOES THIS FILE HAVE A GHC CUSTOMIZATION?
* ghc customizations are listed in the ghCustom.xml file
* a droplet is inserted if the current HWDOCUMENT/@ID matches one listed in ghCustom.xml
	-->
	<xsl:template name="GhCustom">
		<xsl:param name="pCustomLoc" select="''"/>
		<xsl:for-each select="$docGhCustom">
			<xsl:if test="key('kGhCustom', $gvDocHWID)">
				<xsl:choose>
					<xsl:when test="$pCustomLoc='NavStack'">
						<!-- <xsl:for-each select="key('kGhCustom', $gvDocHWID)"> -->
							<xsl:call-template name="ghCustomNav"/>
							<!-- ghCustomNav template is located in navStack.xsl -->
						<!-- </xsl:for-each> -->
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="ghCustomBody"/>
						<!-- ghCustomBody template is located in viewer.xsl -->
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<!-- HW links -->
	<xsl:template match="link.content">
		<xsl:element name="a">
			<xsl:choose>
				<xsl:when test="@name"><!-- a named anchor e.g <a name="foo"> -->
					<xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="href">
						<xsl:choose>
							<!-- a link to a single section document -->
							<xsl:when test="@document-type = 'Definition' or
								@document-type = 'MultiMedia'">
								<!--  or @document-type = 'Calculator' -->
								<xsl:text>javascript:hwPop('</xsl:text><xsl:value-of select="@document-href"/><xsl:text>');</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'/kbase/MomentumTopic.jhtml?docId='"/><xsl:value-of select="@document-href"/>&amp;secId=<xsl:value-of select="@section-href"/>
								<xsl:if test="@name-href != ''">
									<xsl:text>#</xsl:text><xsl:value-of select="@name-href"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="a">
		<xsl:if test="@name">
			<xsl:copy-of select="."/>
		</xsl:if>
	</xsl:template>
	 <xsl:template match="a" mode="ghCustom">
		<xsl:copy-of select="."/>
	</xsl:template> 
	
	<!-- images and slideshow images -->
	<xsl:template match="img | slide.image">
		<xsl:element name="img">
			<xsl:for-each select="@*">
				<xsl:choose>
					<xsl:when test="local-name(.) = 'src'">
						<xsl:attribute name="src">
							<xsl:choose>
								<xsl:when test="../@type = 'medical'">
									<xsl:variable name="vImageType">
										<xsl:choose>
											<xsl:when test="/hw.doc/@hwContent = 'true'">hw</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="concat(translate(substring(/hw.doc/@type,1,1), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), substring-after(/hw.doc/@type, substring(/hw.doc/@type,1,1)))"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<xsl:value-of select="concat('/kbase/hwxml/media/medical/', $vImageType, '/', .)"/>
<!-- <xsl:value-of select="concat( '/kbase/', substring-after(., '../../../'))"/> -->
								</xsl:when>
								<xsl:otherwise>
<!-- <xsl:value-of select="concat( '/kbase/', substring-after(., '../../../'))"/> -->
									<xsl:value-of select="concat('/kbase/hwxml/media/interface/', .)"/>
								</xsl:otherwise>
							</xsl:choose><!--<xsl:value-of select="concat('/kbase/hwxml/media', substring-after(., 'media'))"/>-->
						</xsl:attribute>
					</xsl:when>
					<xsl:when test="local-name(.) = 'type'"/>
					<xsl:otherwise>
						<xsl:copy-of select="."/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
	
	<!-- interactive quizzes (Flash) -->
	<xsl:template match="param[@name='movie']"><!-- add path to flash file location -->
		<xsl:copy>
			<xsl:for-each select="@*">
				<xsl:choose>
					<xsl:when test="local-name(.) = 'value'">
						<xsl:attribute name="value"><xsl:value-of select="concat('/kbase/hwxml/media/flash/hw/', .)"/></xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="."/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="embed">
		<xsl:copy>
			<xsl:for-each select="@*">
				<xsl:choose>
					<xsl:when test="local-name(.) = 'src'">
						<xsl:attribute name="src"><xsl:value-of select="concat('/kbase/hwxml/media/flash/hw/', .)"/></xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="."/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
