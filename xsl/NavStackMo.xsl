<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
encoding=""
iso-8859-1
UTF-16
UTF-8 
-->



<xsl:output method="html" indent="no"/>

<xsl:include href="sharedTemplatesMo.xsl"/>

<!-- *** navStack
*** -->
<!-- <xsl:template name="NavStack"> -->
<xsl:template match="/">

<!-- does this document have custom GHC content?
	<xsl:call-template name="GhCustom">
		<xsl:with-param name="pCustomLoc" select="'NavStack'"/>
	</xsl:call-template> -->
	
	<xsl:if test="/hw.doc/doc.prebuilt/prebuilt.navstack">
		
		<!-- The following do not have NavStacks: Actionset DecisionPoint Profile DecisionPointFrame Frame -->
      <xsl:choose>
      	<xsl:when test="$gvDocType = 'Actionset' or
      		$gvDocType = 'ConditionCenter' or
      		$gvDocType = 'DecisionPoint' or
     		$gvDocType = 'Profile' or
     		$gvDocType = 'DecisionPointFrame' or
     		$gvDocType = 'Frame'"/>
      	<xsl:otherwise>
				<table bgcolor="#f2f2e6" cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td height="1" width="10">
							<img border="0" height="1" width="10" alt="" src="/kbase/images/spacer_ffffff.gif"/>
							<img border="0" height="2" width="10" alt="" src="/kbase/images/spacer_transparent.gif"/>
						</td>
						<td height="1" width="185">
							<img border="0" height="1" width="185" alt="" src="/kbase/images/spacer_ffffff.gif"/>
							<img border="0" height="2" width="185" alt="" src="/kbase/images/spacer_transparent.gif"/>
						</td>
					</tr>
					<tr>
						<td width="10">
							<img border="0" height="1" width="10" alt="" src="/kbase/images/spacer_transparent.gif"/>
						</td>
						<td height="15" class="smalltextb">
							Topic Contents
						</td>
					</tr>
				</table>				
				
				<!-- Save the current section HWID. -->
				<xsl:variable name="vCurrentSectionHWID" select="ancestor-or-self::doc.section/@hwid-content"/>
				
				<table bgcolor="#f2f2e6" cellpadding="0" cellspacing="0" border="0" width="100%">						
					<tr>
						<td valign="top" width="10"><img border="0" height="1" width="10" alt="" src="/kbase/images/spacer_transparent.gif"/></td>
						<td valign="top" width="185">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td><img border="0" height="1" width="10" alt="" src="/kbase/images/spacer_transparent.gif"/></td>
									<td><img border="0" height="1" width="175" alt="" src="/kbase/images/spacer_transparent.gif"/></td>
								</tr>
							
								<xsl:for-each select="/hw.doc/doc.prebuilt/prebuilt.navstack/navstack.entries/navstack.entry">
									<xsl:choose>
										<xsl:when test="$gvDocType = 'Cam' or
											$gvDocType = 'DrugDetail' or
											$gvDocType = 'Multum' or
											$gvDocType = 'Nci' or
											$gvDocType = 'Nord' or 
											$gvDocType = 'OtherDetail' or
											$gvDocType = 'Shc' or
											$gvDocType = 'SurgicalDetail' or
											$gvDocType = 'TestDetail'">
											<tr>																									
												<td valign="middle">																							
													<img align="middle" name="{@section-href}" border="0" height="8" width="7" alt="" src="/kbase/images/bullets/bullet.gif"/>													
													<img border="0" height="15" width="3" alt="" src="/kbase/images/spacer_transparent.gif"/>
												</td>
												<td valign="middle" class="smalltext">
													<a href="#{@section-href}"><xsl:apply-templates/></a>
												</td>
											</tr>
										</xsl:when>
										<xsl:otherwise>
											<tr>											
												<td valign="middle">																															
													<img align="middle" name="{@section-href}" border="0" height="8" width="7" alt="" src="/kbase/images/bullets/bullet.gif"/>
													<img border="0" height="15" width="3" alt="" src="/kbase/images/spacer_transparent.gif"/>
												</td>
												<td valign="middle" class="smalltext">
													<a href="{$gViewer}?docId={$gvDocHWID}&amp;secId={@section-href}"><xsl:apply-templates/></a>
												</td>
											</tr>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								
							</table>
							
						</td>
					</tr>
				</table>
				
			</xsl:otherwise>
		</xsl:choose>
		
		<table bgcolor="#e5e5d9" cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td height="1" width="10">
					<img border="0" height="1" width="10" alt="" src="/kbase/images/spacer_ffffff.gif"/>
					<img border="0" height="4" width="10" alt="" src="/kbase/images/spacer_transparent.gif"/>
				</td>
				<td height="1" width="185">
					<img border="0" height="1" width="185" alt="" src="/kbase/images/spacer_ffffff.gif"/>
					<img border="0" height="4" width="185" alt="" src="/kbase/images/spacer_transparent.gif"/>
				</td>
			</tr>
			<tr>
				<td height="1" width="10"><img border="0" height="5" width="10" alt="" src="/kbase/images/spacer_transparent.gif"/></td>
				<td height="1" width="185"><img border="0" height="5" width="185" alt="" src="/kbase/images/spacer_transparent.gif"/></td>
			</tr>
		</table>
	</xsl:if> <!-- end if test="/hw.doc/doc.prebuilt/prebuilt.navstack" -->
</xsl:template>



</xsl:stylesheet>