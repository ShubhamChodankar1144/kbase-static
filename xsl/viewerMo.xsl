<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
encoding=""
iso-8859-1
UTF-16
UTF-8 
-->
	<xsl:output method="html" indent="no"/>
	<xsl:include href="sharedTemplatesMo.xsl "/>
	<!-- select overall layout of document -->
	<xsl:template name="PageLayout">
		<!-- display the document title in the browser title bar -->
		<xsl:element name="title">
			<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.title"/>
		</xsl:element>
		<!-- add comment to see when view source -->
		<xsl:comment>
	version = <xsl:value-of select="/hw.doc/@version"/>
	hwid-content (doc ID) = <xsl:value-of select="/hw.doc/@hwid-content"/>
	type = <xsl:value-of select="/hw.doc/@type"/>
	rank = <xsl:value-of select="/hw.doc/@rank"/>
		</xsl:comment>
		<xsl:choose>
			<!-- pop up for Symptom doc When-To-See and Actionset doc Quizzes -->
			<xsl:when test="not($wtsId = 'notPassed') or
			not($quizId = 'notPassed')">
				<xsl:call-template name="BodyLayoutC"/>
			</xsl:when>
			<!-- pop up for MultiMedia and Definition docs -->
			<xsl:when test="$gvDocType = 'MultiMedia' or
			$gvDocType = 'Definition' or
			$gvDocType = 'Calculator'">
				<xsl:call-template name="BodyLayoutB"/>
			</xsl:when>
			<!-- full page for the rest -->
			<xsl:otherwise>
				<xsl:call-template name="BodyLayoutA"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- ***
Body for primary documents
*** -->
	<xsl:template name="BodyLayoutA">
		<a name="_top"/>
		<table border="0" cellpadding="0" height="100%" width="100%">
			<tr valign="top">
				<td>
					<xsl:choose>
						<!-- NORD content. -->
						<xsl:when test="$gvDocType = 'Nord'">
							<h4>National Organization for Rare Disorders, Inc.</h4>
						</xsl:when>
						<!-- SHC content. -->
						<xsl:when test="$gvDocType = 'Shc'">
							<h4>Self Help Clearinghouse</h4>
						</xsl:when>
					</xsl:choose>
					<!-- Output the document title. -->
					<h2>
						<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.title"/>
					</h2>
						<!-- does this document have custom GHC content? -->
						 
					<!-- decide what to render and how to render it -->
					<xsl:choose>
						<!-- the following doctypes render all sections on one page -->
						<xsl:when test="$gvDocType = 'Cam' or
						$gvDocType = 'ConditionCenter' or
						$gvDocType = 'DecisionPoint' or
						$gvDocType = 'DecisionPointFrame' or
						$gvDocType = 'DrugDetail' or
						$gvDocType = 'Frame' or
						$gvDocType = 'Multum' or
						$gvDocType = 'Nci' or
						$gvDocType = 'Nord' or 
						$gvDocType = 'OtherDetail' or
						$gvDocType = 'Profile' or
						$gvDocType = 'Shc' or
						$gvDocType = 'SurgicalDetail' or
						$gvDocType = 'TestDetail'">
							<!-- $gvDocType = 'Actionset' or -->
							<xsl:for-each select="doc.sections/section.include/doc.section|doc.sections/doc.section">
								<xsl:choose>
									<!-- Suppress the references and credits sections for details and ConditionCenter. -->
									<xsl:when test="(contains($gvDocType, 'Detail') or $gvDocType='ConditionCenter') and @hwid-content = concat(/hw.doc/@hwid-content, '-Credits')"/>
									<xsl:when test="$gvDocType = 'Frame' and 
									(@hwid-content = concat(/hw.doc/@hwid-content, '-Credits'))"/>
									<!--  or @hwid-content = concat(/hw.doc/@hwid-content, '-Bib') -->
									<xsl:otherwise>
										<!-- if there is a section title that is different than the main doc title, show it -->
										<xsl:if test="section.meta-data/meta-data.title">
											<xsl:if test="section.meta-data/meta-data.title != /hw.doc/doc.meta-data/meta-data.title">
												<a name="{@hwid-content}"/>
												<h3>
													<xsl:value-of select="section.meta-data/meta-data.title"/>
												</h3>
											</xsl:if>
										</xsl:if>
										<!-- Process all of children of the current section. -->
										<xsl:apply-templates/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
							<!-- render top of page link -->
							<div id="prevTopNext">
								<div id="top">
									<a href="#_top" class="smalltextb" style="text-decoration:none;">
										<img src="/kbase/images/arrows/arrow_up_ffffff.gif" width="8" height="7" alt=""/> Top of Page</a>
								</div>
							</div>
							<xsl:choose>
								<xsl:when test="/hw.doc/@hwContent = 'true' and /hw.doc/@type != 'Support'"><xsl:call-template name="doUrac"/></xsl:when>
								<xsl:when test="/hw.doc/@type = 'Multum'"><xsl:call-template name="doMultDisc"/></xsl:when>
								<xsl:otherwise/>
							</xsl:choose>
							<!-- dummy hwSecId to avoid javascript error -->
							<xsl:element name="script">
								<xsl:attribute name="type">text/javascript</xsl:attribute>
								<xsl:text>hwSecId = 'none';</xsl:text>
							</xsl:element>
						</xsl:when>
						<!-- end render all section on one page -->
						<!-- otherwise render each section on a separate page -->
						<xsl:otherwise>
							<xsl:choose>
								<!--if no section Id is passed, render first section of doc -->
								<xsl:when test="$gvPassedSecId = 'notPassed' or $gvPassedSecId = $gvDocHWID">
									<!-- render section title -->
									<xsl:if test="/hw.doc/doc.sections/doc.section[1]/section.meta-data/meta-data.title">
										<xsl:if test="/hw.doc/doc.sections/doc.section[1]/section.meta-data/meta-data.title != /hw.doc/doc.meta-data/meta-data.title">
											<h3>
												<xsl:value-of select="/hw.doc/doc.sections/doc.section[1]/section.meta-data/meta-data.title"/>
											</h3>
										</xsl:if>
									</xsl:if>
									<xsl:apply-templates select="/hw.doc/doc.sections/doc.section[1]"/>
								</xsl:when>
								<!--if section Id is passed, render specified section of doc -->
								<xsl:otherwise>
									<!-- render section title -->
									<xsl:if test="/hw.doc/doc.sections/doc.section[@hwid-content = $gvPassedSecId]/section.meta-data/meta-data.title">
										<xsl:if test="/hw.doc/doc.sections/doc.section[@hwid-content = $gvPassedSecId]/section.meta-data/meta-data.title != /hw.doc/doc.meta-data/meta-data.title">
											<h3>
												<xsl:value-of select="/hw.doc/doc.sections/doc.section[@hwid-content = $gvPassedSecId]/section.meta-data/meta-data.title"/>
											</h3>
										</xsl:if>
									</xsl:if>
									<!-- render the section content -->
									 
									<xsl:choose>
										<!-- credits sections are hand-built from meta-data  -->
										<xsl:when test="contains($gvPassedSecId, '-Credits')">
											<xsl:call-template name="doCredits"/>
										</xsl:when>
										<!--  all other sections are taken straight from XML source  -->
										<xsl:otherwise>
											<xsl:apply-templates select="/hw.doc/doc.sections/doc.section[@hwid-content = $gvPassedSecId]"/>
										</xsl:otherwise>
									</xsl:choose>
									
									<!--<xsl:apply-templates select="/hw.doc/doc.sections/doc.section[@hwid-content = $gvPassedSecId]"/>-->
								</xsl:otherwise>
							</xsl:choose>
							<xsl:call-template name="doPrevNext"/>
							<!-- urac (credits) info at bottom of Healthwise-authored sections, except Credits sections -->
							<xsl:if test="/hw.doc/@hwContent = 'true' and /hw.doc/@type != 'Support'">
								<xsl:if test="not(contains($gvPassedSecId, '-Credits'))">
									<xsl:call-template name="doUrac"/>
								</xsl:if>
							</xsl:if>
						</xsl:otherwise>
						<!-- end render sections on separate page -->
					</xsl:choose>
					<!-- end render sections -->
				</td>
			</tr>
		</table>
				 
	</xsl:template>
	<!-- end BodyLayoutA -->
	<!-- ***
Body for popup documents
*** -->
	<xsl:template name="BodyLayoutB">
		<!-- Output the document title. -->
		<h2>
			<xsl:apply-templates select="/hw.doc/doc.meta-data/meta-data.title"/>
		</h2>
		<xsl:for-each select="doc.sections/section.include/doc.section|doc.sections/doc.section">
			<xsl:choose>
				<!-- Suppress the references and credits sections for details. -->
				<xsl:when test="@hwid-content = concat(/hw.doc/@hwid-content, '-Credits') or @hwid-content = concat(/hw.doc/@hwid-content, '-Bib')"/>
				<xsl:otherwise>
					<!-- Process all of children of the current section. -->
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<!-- Build Metadata section for review. 
	<xsl:if test="//meta-data.collections">
		<xsl:call-template name="MetaData"/>
	</xsl:if> -->
	</xsl:template>
	<!-- end BodyLayoutB -->
	<xsl:template name="BodyLayoutC">
		<xsl:choose>
			<!-- render contents of Symptom doc when-to-see -->
			<xsl:when test="not($wtsId = 'notPassed')">
				<xsl:apply-templates select="/hw.doc/doc.sections//cys.when-to-see[@hwid-content = $wtsId]"/>
			</xsl:when>
			<!-- render Actionset quiz answer -->
			<xsl:when test="not($quizId = 'notPassed')">
				<xsl:call-template name="quizAnswer"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!-- end BodyLayoutC -->
	<!--
Utility Templates
-->
	<!-- Render GHC custom content at top of Main body column -->
	<xsl:template name="ghCustomBody">
	</xsl:template>
	<!-- end ghCustomBody -->
	<xsl:template name="doPrevNext">
		<!-- render the previous, top of page, and next links -->
		<xsl:variable name="vTheSection">
			<xsl:choose>
				<xsl:when test="$gvPassedSecId = 'notPassed'">
					<xsl:value-of select="/hw.doc/doc.sections/doc.section[1]/@hwid-content"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$gvPassedSecId"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- hwSecId for NavStack js -->
		<xsl:element name="script">
			<xsl:attribute name="type">text/javascript</xsl:attribute>
			<xsl:text>hwSecId = '</xsl:text>
			<xsl:value-of select="$vTheSection"/>
			<xsl:text>';</xsl:text>
		</xsl:element>
		<xsl:variable name="vPrevLink">
			<xsl:for-each select="/hw.doc/doc.sections/doc.section[@hwid-content = $vTheSection]">
				<xsl:value-of select="preceding-sibling::*[1]/@hwid-content"/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:variable name="vNextLink">
			<xsl:for-each select="/hw.doc/doc.sections/doc.section[@hwid-content = $vTheSection]">
				<xsl:value-of select="following-sibling::*[1]/@hwid-content"/>
			</xsl:for-each>
		</xsl:variable>
		<div id="prevTopNext">
			<div id="prev">
				<xsl:if test="$vPrevLink != ''">
					<a href="{concat('/kbase/MomentumTopic.jhtml?docId=', $gvDocHWID, '&amp;secId=', $vPrevLink)}" class="smalltextb" style="text-decoration:none;">
						<img src="/kbase/images/arrows/arrow_left_ffffff.gif" width="8" height="7" alt=""/> Previous
				</a>
				</xsl:if>
			</div>
			<div id="top">
				<a href="#_top" class="smalltextb" style="text-decoration:none;">
					<img src="/kbase/images/arrows/arrow_up_ffffff.gif" width="8" height="7" alt=""/> Top
			</a>
			</div>
			<div id="next">
				<xsl:if test="$vNextLink!= ''">
					<a href="{concat('/kbase/MomentumTopic.jhtml?docId=', $gvDocHWID, '&amp;secId=', $vNextLink)}" class="smalltextb" style="text-decoration:none;">
					Next <img src="/kbase/images/arrows/arrow_right_ffffff.gif" width="8" height="7" alt=""/>
					</a>
				</xsl:if>
			</div>
		</div>
	</xsl:template>
	<!-- end doPrevNext -->
	<xsl:template name="doUrac">
		<div id="uracContainer">
			<div class="uracRow">
				<div class="uracCol1">Author:</div>
				<div class="uracCol2">
					<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.simple-collection/meta-data.simple[@name='author']"/>
				</div>
			</div>
			<div class="uracRow">
				<div class="uracCol1">Medical Review:</div>
				<div class="uracCol2">
					<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.simple-collection/meta-data.simple[@name='primary-medical-reviewer']"/>
				</div>
			</div>
			<xsl:if test="/hw.doc/doc.meta-data/meta-data.simple-collection/meta-data.simple[@name='specialist-medical-reviewer']">
				<div class="uracRow">
					<div class="uracCol1"/>
					<div class="uracCol2">
						<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.simple-collection/meta-data.simple[@name='specialist-medical-reviewer']"/>
					</div>
				</div>
			</xsl:if>
			<div class="uracRow">
				<div class="uracCol1">Last Updated:</div>
				<div class="uracCol2">
					<xsl:value-of select="/hw.doc/doc.meta-data/meta-data.simple-collection/meta-data.simple[@name='ReleaseDate']"/>
				</div>
			</div>
		</div>
	</xsl:template>
	<!-- end doUrac -->
	<xsl:template name="doMultDisc">
		<div id="uracContainer">
			Your use of the content provided in this service indicates that you have read, understood, and agree to the 
			<a href="javascript:hwPop('multdisc')">Multum End-User License Agreement</a>.
		</div>
	</xsl:template>
	<xsl:template name="doCredits">
		<table border="0" cellpadding="5">
			<xsl:for-each select="/hw.doc/doc.meta-data/meta-data.credits/credits.person">
				<tr>
					<td valign="top">
						<b>
						<xsl:choose>
							<xsl:when test="@role='author-name'">Author</xsl:when>
							<xsl:when test="@role='editor-name'">Editor</xsl:when>
							<xsl:when test="@role='associate-editor-name'">Associate Editor</xsl:when>
							<xsl:when test="@role='primary-medical-reviewer'">Primary Medical Reviewer</xsl:when>
							<xsl:when test="@role='secondary-medical-reviewer'">Specialist Medical Reviewer</xsl:when>
						</xsl:choose>
						</b>
					</td>
					<td>
					<xsl:choose>
					<!-- include a link to reviewer's profile if it exists -->
						<xsl:when test="@document-href">
							<a href="{concat('MomentumTopic.jhtml?docId=', @document-href)}"><xsl:value-of select="." /></a>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
					</xsl:choose>
					</td>
				</tr>
			</xsl:for-each>
			<xsl:if test="/hw.doc/doc.meta-data/meta-data.simple-collection/meta-data.simple[@name='ReleaseDate']">
				<tr>
					<td>
						<b>Last Updated</b>
					</td>
					<td><xsl:value-of select="/hw.doc/doc.meta-data/meta-data.simple-collection/meta-data.simple[@name='ReleaseDate']" /></td>
				</tr>
			</xsl:if>
		</table>
	</xsl:template>
	<!-- end doCredits -->
	<!--
================================================================================
 Section 3.1 : Default Element Template
================================================================================
    -->
	<!--
    Default Element Template - Used to handle the majority of the XHTML elements
    that occur within Healthwise content.
    -->
	<xsl:template match="*">
		<xsl:copy>
			<xsl:for-each select="@*">
				<xsl:choose>
					<xsl:when test="self::class">
						<xsl:choose>
							<!-- Keep these classes as they will be handled via CSS or applications. -->
							<xsl:when test=". = 'Note'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Red'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Size1'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Size2'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Size3'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Size4'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Sizeminus'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Sizeplus'"><xsl:copy-of select="."/></xsl:when>
							<xsl:when test=". = 'Small'"><xsl:copy-of select="."/></xsl:when>
							<!-- Suppress these classes. -->
							<xsl:when test=". = 'NoWebPrint'"/>
							<xsl:otherwise>
								<!-- Throw an error that an unknown class was encountered -->
								<xsl:message terminate="yes">An element with an unknown class, "<xsl:value-of select="."/>", was encountered.</xsl:message>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="."/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="div">
		<xsl:choose>
		<!-- special processing for ConditionCenter a.k.a. "Learning Centers" topics, which contain a list of relevant topics -->
			<xsl:when test="@class = 'ConditionCenterTitle'">
				<h4><xsl:value-of select="."/></h4>
				<ul>
					<xsl:for-each select="following-sibling::div[@class='ConditionCenterItem']">
						<li><xsl:apply-templates/></li>
					</xsl:for-each>
				</ul>
			</xsl:when>
			<xsl:when test="@class = 'ConditionCenterItem'"/>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!--
================================================================================
 Section 3.2 : HW Element Templates
================================================================================
-->
	<xsl:template match="/hw.doc">
		<xsl:call-template name="PageLayout"/>
	</xsl:template>
	<!-- Suppress meta-data content -->
	<xsl:template match="doc.meta-data|section.meta-data"/>
	<xsl:template match="meta-data.title">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="prebuilt.disclaimer | prebuilt.creditsfooter">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="section.std | section.emer | section.cys | section.qdoc">
		<xsl:apply-templates/>
	</xsl:template>
	<!-- Suppress QDoc results elements -->
	<xsl:template match="qdoc.results"/>
	<xsl:template match="block.drugs">
		<table border="1" cellpadding="5">
			<tr>
				<!-- Are there any brand names? -->
				<xsl:if test=".//drug.brand-names">
					<th>Brand Name</th>
				</xsl:if>
				<!-- Are there any generic names? -->
				<xsl:if test=".//drug.generic-name">
					<th>Generic Name</th>
				</xsl:if>
				<th>Chemical Name</th>
			</tr>
			<!-- Determine the number of columns in the drug table -->
			<xsl:variable name="vDrugCols">
				<xsl:choose>
					<xsl:when test=".//drug.generic-name and .//drug.brand-names">3</xsl:when>
					<xsl:when test=".//drug.generic-name">2</xsl:when>
					<xsl:when test=".//drug.brand-names">2</xsl:when>
					<xsl:otherwise>1</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:apply-templates>
				<xsl:with-param name="pDrugCols" select="$vDrugCols"/>
			</xsl:apply-templates>
		</table>
	</xsl:template>
	<xsl:template match="block.drug">
		<xsl:param name="pDrugCols"/>
		<tr>
			<xsl:choose>
				<xsl:when test="$pDrugCols = '3' and drug.generic-name and drug.brand-names">
					<td>
						<b>
							<xsl:apply-templates select="drug.brand-names"/>
						</b>
					</td>
					<td>
						<b>
							<xsl:apply-templates select="drug.generic-name"/>
						</b>
					</td>
				</xsl:when>
				<xsl:when test="$pDrugCols = '3' and drug.generic-name">
					<td>&#160;</td>
					<td>
						<b>
							<xsl:apply-templates select="drug.generic-name"/>
						</b>
					</td>
				</xsl:when>
				<xsl:when test="$pDrugCols = '3' and drug.brand-names">
					<td>
						<b>
							<xsl:apply-templates select="drug.brand-names"/>
						</b>
					</td>
					<td>&#160;</td>
				</xsl:when>
				<xsl:when test="$pDrugCols = '2' and drug.generic-name">
					<td>
						<b>
							<xsl:apply-templates select="drug.generic-name"/>
						</b>
					</td>
				</xsl:when>
				<xsl:when test="$pDrugCols = '2' and drug.brand-names">
					<td>
						<b>
							<xsl:apply-templates select="drug.brand-names"/>
						</b>
					</td>
				</xsl:when>
			</xsl:choose>
			<td>
				<b>
					<xsl:apply-templates select="drug.chemical-name"/>
				</b>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="drug.brand-name">
		<xsl:apply-templates/>
		<xsl:if test="position() != last() and count(../drug.brand-name) > 1">, </xsl:if>
	</xsl:template>
	<!-- Quiz questions are numbered. -->
	<xsl:template match="quiz.questions">
		<ol>
			<xsl:apply-templates/>
		</ol>
	</xsl:template>
	<xsl:template match="cys.chart-text"/>
	<xsl:template match="question.text">
		<xsl:choose>
			<xsl:when test="parent::cys.question">
				<tr>
					<td align="center" width="25" valign="top">
						<xsl:if test="following-sibling::cys.when-to-see[1]//cys.severity-group/@severity = 's1'">
							<img src="/kbase/hwxml/media/interface/red.gif" height="20" width="20" align="middle"/>
						</xsl:if>
					</td>
					<td align="center" width="50" valign="top">
						<!-- <a href="#{following-sibling::cys.when-to-see[1]/@hwid-content}">Yes</a> -->
						<!-- <a href="{concat($gViewer, '?docId=', $gvDocHWID, '&amp;wtsId=', following-sibling::cys.when-to-see[1]/@hwid-content)}">Yes</a> -->
						<a href="javascript:hwPop('{$gvDocHWID}&amp;wtsId={following-sibling::cys.when-to-see[1]/@hwid-content}');">Yes</a>
					</td>
					<td>
						<xsl:apply-templates/>
					</td>
				</tr>
			</xsl:when>
			<xsl:when test="parent::severity.question">
				<tr>
					<td align="center" valign="top" width="50">
						<!-- <a href="javascript:hwPop('{ancestor::cys.severity-group/@href}');">Yes</a> -->
						<a href="javascript:hwPop('{substring-before(ancestor::cys.severity-group/@section-href, '-sec')}');">Yes</a>
					</td>
					<td>
						<xsl:apply-templates/>
					</td>
				</tr>
			</xsl:when>
			<xsl:when test="parent::quiz.question">
				<li>
					<xsl:apply-templates/>
				</li>
			</xsl:when>
			<xsl:when test="parent::section.emer">
				<table border="0">
					<tr>
						<td align="center" width="50" valign="top">
							<a href="javascript:hwPop('{parent::section.emer/@document-href}');">Yes</a>
						</td>
						<td valign="top">
							<xsl:apply-templates/>
						</td>
					</tr>
				</table>
			</xsl:when>
			<xsl:otherwise>
				<!-- Throw an error that an unexpected condition was encountered -->
				<xsl:message terminate="yes">An unexpected condition was encountered processing element: <xsl:value-of select="local-name(.)"/>. Closest HWID: <xsl:value-of select="ancestor-or-self::*[@hwid-content]/@hwid-content"/>".</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Quiz answers are lettered. -->
	<xsl:template match="question.answers">
		<ol type="a">
			<xsl:apply-templates/>
		</ol>
	</xsl:template>
	<!-- During document order processing, only need to output the answer text and not the explanation. -->
	<xsl:template match="question.answer">
		<xsl:apply-templates select="answer.text"/>
	</xsl:template>
	<xsl:template match="answer.text">
		<li>
			<!-- <a href="{concat($gViewer, '?docId=', $gvDocHWID, '&amp;quizId=', parent::question.answer/@hwid-content)}"> -->
			<a href="javascript:hwPop('{$gvDocHWID}&amp;quizId={parent::question.answer/@hwid-content}');">
				<xsl:apply-templates/>
			</a>
		</li>
	</xsl:template>
	<xsl:template name="quizAnswer">
		<xsl:for-each select="/hw.doc/doc.sections//question.answer[@hwid-content = $quizId]/answer.explanation">
			<xsl:choose>
				<xsl:when test="parent::question.answer/@correct = 'yes'">
					<p>This answer is correct.</p>
				</xsl:when>
				<xsl:when test="parent::question.answer/@correct = 'no'">
					<p>This answer is incorrect.</p>
				</xsl:when>
				<xsl:when test="parent::question.answer/@correct = 'all'">
					<p>All answers are correct.</p>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates/>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="cys.questions">
		<table border="0">
			<xsl:apply-templates/>
		</table>
	</xsl:template>
	<!-- During document order processing, only need to output the question.text and not cys.chart-text or cys.when-to-see. -->
	<xsl:template match="cys.question">
		<xsl:apply-templates select="question.text"/>
	</xsl:template>
	<xsl:template match="cys.severity-groups">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="cys.severity-group">
		<table border="2" cellpadding="5" cellspacing="0" width="80%">
			<!-- Set the correct colors for the table based on the severity. -->
			<xsl:choose>
				<xsl:when test="@severity = 's1'">
					<xsl:attribute name="bordercolordark">DARKRED</xsl:attribute>
					<xsl:attribute name="bordercolorlight">LIGHTCORAL</xsl:attribute>
				</xsl:when>
				<xsl:when test="@severity = 's2'">
					<xsl:attribute name="bordercolordark">GOLD</xsl:attribute>
					<xsl:attribute name="bordercolorlight">LIGHTYELLOW</xsl:attribute>
				</xsl:when>
				<xsl:when test="@severity = 's3'">
					<xsl:attribute name="bordercolordark">GREEN</xsl:attribute>
					<xsl:attribute name="bordercolorlight">LIGHTGREEN</xsl:attribute>
				</xsl:when>
				<xsl:when test="@severity = 's4'">
					<xsl:attribute name="bordercolordark">BLACK</xsl:attribute>
					<xsl:attribute name="bordercolorlight">C0C0C0</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:message terminate="yes">Unexpected severity value while processing cys.severity-group: <xsl:value-of select="@severity"/>.</xsl:message>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates/>
		</table>
		<br/>
	</xsl:template>
	<xsl:template match="severity-group.header">
		<tr>
			<td valign="top" align="center">
				<a href="#{../@section-href}">
					<xsl:choose>
						<xsl:when test="../@severity = 's1'">
							<img src="/kbase/hwxml/media/interface/red.gif" border="0" alt="Red"/>
						</xsl:when>
						<xsl:when test="../@severity = 's2'">
							<img src="/kbase/hwxml/media/interface/yellow.gif" border="0" alt="Yellow"/>
						</xsl:when>
						<xsl:when test="../@severity = 's3'">
							<img src="/kbase/hwxml/media/interface/green.gif" border="0" alt="Green"/>
						</xsl:when>
						<xsl:when test="../@severity = 's4'">
							<img src="/kbase/hwxml/media/interface/black.gif" border="0" alt="Black"/>
						</xsl:when>
					</xsl:choose>
				</a>
			</td>
			<td colspan="2">
				<xsl:apply-templates/>
			</td>
		</tr>
	</xsl:template>
	<!-- During document order processing, only need to output the question.text and not cys.chart-text. -->
	<xsl:template match="severity.question">
		<xsl:apply-templates select="question.text"/>
	</xsl:template>
	<!-- open the popoffwindow for the cit-ref -->
	<xsl:template match="inline.cit-ref">
		<sup>
			<a href="#{@href}">
				<xsl:number level="any"/>
			</a>
		</sup>
	</xsl:template>
	<xsl:template match="inline.emer-num">
		<span class="EmergencyNumber">
			<xsl:text> </xsl:text>
			<xsl:value-of select="$emergencyNumber"/>
			<xsl:text> </xsl:text>
		</span>
	</xsl:template>
	<xsl:template match="inline.measurement">
		<xsl:choose>
			<xsl:when test="@unit2">
				<xsl:choose>
					<xsl:when test="@unit1 = 'F'">
						<xsl:value-of select="concat(@initval, ' degrees ', @unit1, ' (', @convval, ' degrees ', @unit2, ')')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="concat(@initval, ' ', @unit1, ' (', @convval, ' ', @unit2, ')')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(@initval, ' ', @unit1)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- <xsl:template match="blockquote">
	<xsl:copy-of select="./*"/>
</xsl:template> -->
	<xsl:template match="link.external">
		<!-- don't forget the image media/interface/form.gif -->
		<xsl:element name="a">
			<xsl:attribute name="href">
				<xsl:choose>
					<xsl:when test="@type = 'entrypage' and @href = 'DEFAULT'">/kbase/index.jhtml</xsl:when>
					<xsl:otherwise><xsl:value-of select="concat('/kbase/hwxml/media/pdf/hw/', @href)"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<!-- slideshows -->
	<xsl:template match="block.slideshow">
		<xsl:apply-templates select="slideshow.slide[number($gvSlideNumber)]"/>
	</xsl:template>
	
	<xsl:template match="slideshow.slide">
		<div id="slideshow">
			<a name="showNav"/>
			<xsl:call-template name="slideshowNav"/>
			<xsl:apply-templates/>
			<xsl:call-template name="slideshowNav"/>
		</div>
	</xsl:template>
	
	<xsl:template match="slide.title">
		<a name="slideTitle"/>
		<h3><xsl:value-of select="."/></h3>
	</xsl:template>
	
	<!-- template for slide.image is in sharedTemplates.xsl because it has the same rules as img -->
	
	<xsl:template match="slide.description">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template name="slideshowNav">
		<!-- slide x of n -->
		<div class="slideshowNav">
			<!-- previous -->
			<xsl:if test="preceding-sibling::slideshow.slide">
				<xsl:variable name="prevSlide" select="$gvSlideNumber -1"/>
				<a class="prevSlide" href="MomentumPopup.jhtml?docId={$gvDocHWID}&amp;slideNum={$prevSlide}#showNav">Previous</a>
			</xsl:if>
			<span class="numSlides">
				<xsl:value-of select="$gvSlideNumber"/> of <xsl:value-of select="count(../slideshow.slide)"/>
			</span>
			<!-- next -->
			<xsl:if test="following-sibling::slideshow.slide">
				<xsl:variable name="NextSlide" select="$gvSlideNumber +1"/>
				<a class="nextSlide" href="MomentumPopup.jhtml?docId={$gvDocHWID}&amp;slideNum={$NextSlide}#showNav">Next</a>
			</xsl:if>
		</div>
	</xsl:template>

	<!-- supress comments -->
	<xsl:template match="comment()"/>
	<!-- supress processing-instruction -->
	<xsl:template match="processing-instruction()"/>
</xsl:stylesheet>
